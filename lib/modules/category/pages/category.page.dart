
import 'package:flutter/material.dart';
import 'package:ichillan_app/modules/category/widgets/category_list.widget.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Categorías', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: Container()
      ),
      body: CategoryListWidget(),
    );
  }
}
