import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/category/widgets/category_item.widget.dart';
import 'package:ichillan_app/modules/culture/pages/culture.page.dart';
import 'package:ichillan_app/modules/education/pages/education.page.dart';
import 'package:ichillan_app/modules/entertainment/pages/entertainment.page.dart';
import 'package:ichillan_app/modules/entrepreneurship/pages/entrepreneurship.page.dart';
import 'package:ichillan_app/modules/fuel_station/pages/fuel_station.page.dart';
import 'package:ichillan_app/modules/hotel/pages/hotel.page.dart';
import 'package:ichillan_app/modules/park/pages/park.page.dart';
import 'package:ichillan_app/modules/pharmacy/pages/pharmacy.page.dart';
import 'package:ichillan_app/modules/restaurant/pages/restaurant.page.dart';
import 'package:ichillan_app/modules/sport/pages/sport.page.dart';
import 'package:ichillan_app/modules/supermarket/pages/supermarket.page.dart';
import 'package:ichillan_app/modules/veterinary/pages/veterinary.page.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class CategoryListWidget extends StatefulWidget {
  @override
  _CategoryListWidgetState createState() => _CategoryListWidgetState();
}

class _CategoryListWidgetState extends State<CategoryListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GridView(
          padding: EdgeInsets.all(5),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, crossAxisSpacing: 10, mainAxisSpacing: 10),
          children: [
            GestureDetector(
                onTap: (){
                  Get.to(() => EntrepreneurshipPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.rocket, name: 'Emprendimiento')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => SportPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.running, name: 'Deportes')
            ),
            GestureDetector(
              onTap: (){
                Get.to(() => EntertainmentPage());
              },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.cocktail, name: 'Entretenimiento')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => CulturePage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.cameraRetro, name: 'Cultura')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => ParkPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.tree, name: 'Plazas')
            ),
            GestureDetector(
              onTap: (){
                Get.to(() => HotelPage());
              },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.hSquare, name: 'Hotelería')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => FuelStationPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.gasPump, name: 'Estaciones de bencina')
            ),
            GestureDetector(
              onTap: (){
                Get.to(() => RestaurantPage());
              },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.utensils, name: 'Restaurantes')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => EducationPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.school, name: 'Educación')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => PharmacyPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.pills, name: 'Farmacia')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => SupermarketPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.shoppingCart, name: 'Supermercado')
            ),
            GestureDetector(
                onTap: (){
                  Get.to(() => VeterinaryPage());
                },
                child: CategoryItemWidget(fontAwesomeIcons: FontAwesomeIcons.cat, name: 'Veterinaria')
            ),


          ],

        ));
  }
}
