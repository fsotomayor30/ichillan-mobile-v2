import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class CategoryItemWidget extends StatefulWidget {
 final IconData fontAwesomeIcons;
  final String name;

  CategoryItemWidget({@required this.fontAwesomeIcons, @required this.name});

  @override
  _CategoryItemWidgetState createState() => _CategoryItemWidgetState();
}

class _CategoryItemWidgetState extends State<CategoryItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: COLOR_PRIMARY,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FaIcon(widget.fontAwesomeIcons, size: 40, color: Colors.white,),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(widget.name, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w300, fontSize: 10, color: Colors.white)),
          )
        ],
      ),
    );
  }
}
