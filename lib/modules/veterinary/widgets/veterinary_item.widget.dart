import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/veterinary.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class VeterinaryItemWidget extends StatefulWidget {
  final VeterinaryEntity veterinaryEntity;

  VeterinaryItemWidget({@required this.veterinaryEntity});

  @override
  _VeterinaryItemWidgetState createState() => _VeterinaryItemWidgetState();
}

class _VeterinaryItemWidgetState extends State<VeterinaryItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        leading: Container(
          width: 40,
          height: 40,
          child: CircleAvatar(
            radius: 40,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage(VETERINARY_ICON),
          ),
        ),
        title: Text(
          widget.veterinaryEntity.name,
          style: TextStyle(fontSize: 14, color: Color(0xFF707070)),
        ),
        subtitle: Text(
          widget.veterinaryEntity.address,
          style: TextStyle(fontSize: 10, color: Color(0xFF707070)),
        ),
        trailing:
            FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY),
        onTap: () {
          _sweetSheet.show(
              context: context,
              description: Text(
                'Que quieres hacer?',
                style: TextStyle(color: Color(0xff2D3748)),
              ),
              color: CustomSheetColor(
                main: Colors.white,
                accent: Colors.blue,
                icon: Colors.blue,
              ),
              icon: FontAwesomeIcons.info,
              actions: [
                SweetSheetAction(
                  onPressed: () async {
                    if (await canLaunch(
                        'https://www.google.com/maps/search/?api=1&query=' +
                            widget.veterinaryEntity.latitude.toString() +
                            ',' +
                            widget.veterinaryEntity.longitude.toString())) {
                      await launch(
                          'https://www.google.com/maps/search/?api=1&query=' +
                              widget.veterinaryEntity.latitude.toString() +
                              ',' +
                              widget.veterinaryEntity.longitude.toString());
                    } else {
                      throw 'Could not launch ' +
                          'https://www.google.com/maps/search/?api=1&query=' +
                          widget.veterinaryEntity.latitude.toString() +
                          ',' +
                          widget.veterinaryEntity.longitude.toString();
                    }
                  },
                  title: 'IR',
                ),
                SweetSheetAction(
                  onPressed: () async {
                    await launch('tel:' + widget.veterinaryEntity.phone);
                  },
                  title: 'LLAMAR',
                ),
              ]);
        },
      ),
    );
  }
}
