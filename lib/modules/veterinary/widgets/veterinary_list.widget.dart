import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/veterinary/controllers/veterinary.controller.dart';
import 'package:ichillan_app/modules/veterinary/widgets/veterinary_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class VeterinaryListWidget extends StatefulWidget {
  @override
  _VeterinaryListWidgetState createState() => _VeterinaryListWidgetState();
}

class _VeterinaryListWidgetState extends State<VeterinaryListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listVeterinary',
            init: VeterinaryController(),
            builder: (___) => ListView.builder(
              itemCount: _.vets.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.vets[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.vets[index].address.toLowerCase().contains(___.search.toLowerCase()))){
                  return VeterinaryItemWidget(veterinaryEntity: _.vets[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
