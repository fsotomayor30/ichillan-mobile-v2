import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/veterinary/widgets/input_search_veterinary.widget.dart';
import 'package:ichillan_app/modules/veterinary/widgets/veterinary_list.widget.dart';

class VeterinaryPage extends StatefulWidget {
  @override
  _VeterinaryPageState createState() => _VeterinaryPageState();
}

class _VeterinaryPageState extends State<VeterinaryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Veterinarias', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchVeterinaryWidget(),
          Expanded(
            child:VeterinaryListWidget(),
          )
        ],
      ),
    );
  }
}
