import 'package:get/get.dart';

class VeterinaryController extends GetxController{
  String _search;
  String get search => _search;

  void setSearch(String search){
    _search = search;
    update(['listVeterinary']);
  }
}