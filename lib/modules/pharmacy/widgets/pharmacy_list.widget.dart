import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/pharmacy/controllers/pharmacy.controller.dart';
import 'package:ichillan_app/modules/pharmacy/widgets/pharmacy_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class PharmacyListWidget extends StatefulWidget {
  @override
  _PharmacyListWidgetState createState() => _PharmacyListWidgetState();
}

class _PharmacyListWidgetState extends State<PharmacyListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listPharmacy',
            init: PharmacyController(),
            builder: (___) => ListView.builder(
              itemCount: _.pharmacies.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.pharmacies[index].nombre.toLowerCase().contains(___.search.toLowerCase())) ||(_.pharmacies[index].direccion.toLowerCase().contains(___.search.toLowerCase()))){
                  return PharmacyItemWidget(pharmacyEntity: _.pharmacies[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
