import 'package:get/get_state_manager/get_state_manager.dart';

class PharmacyController extends GetxController{
  String _search;
  String get search => _search;

  void setSearch(String search){
    _search = search;
    update(['listPharmacy']);
  }
}