import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/pharmacy/widgets/input_search_pharmacy.widget.dart';
import 'package:ichillan_app/modules/pharmacy/widgets/pharmacy_list.widget.dart';

class PharmacyPage extends StatefulWidget {
  @override
  _PharmacyPageState createState() => _PharmacyPageState();
}

class _PharmacyPageState extends State<PharmacyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Farmacias', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchPharmacyWidget(),
          Expanded(
            child:PharmacyListWidget(),
          )
        ],
      ),
    );
  }
}
