import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/entertainment/widgets/entertainment_list.widget.dart';
import 'package:ichillan_app/modules/entertainment/widgets/input_search_entertainment.widget.dart';

class EntertainmentPage extends StatefulWidget {
  @override
  _EntertainmentPageState createState() => _EntertainmentPageState();
}

class _EntertainmentPageState extends State<EntertainmentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Entertainment', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchEntertainmentWidget(),
          Expanded(
            child:EntertainmentListWidget(),
          )
        ],
      ),
    );
  }
}
