import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/entertainment/controllers/entertainment.controller.dart';
import 'package:ichillan_app/modules/entertainment/widgets/entertainment_item.widget.dart';
import 'package:ichillan_app/modules/restaurant/controllers/restaurant.controller.dart';
import 'package:ichillan_app/modules/restaurant/widgets/restaurant_item.widget.dart';
import 'package:ichillan_app/modules/sport/controllers/sport.controller.dart';
import 'package:ichillan_app/modules/sport/widgets/sport_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class EntertainmentListWidget extends StatefulWidget {
  @override
  _EntertainmentListWidgetState createState() => _EntertainmentListWidgetState();
}

class _EntertainmentListWidgetState extends State<EntertainmentListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listEntertainment',
            init: EntertainmentController(),
            builder: (___) => ListView.builder(
              itemCount: _.entertainments.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.entertainments[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.entertainments[index].address.toLowerCase().contains(___.search.toLowerCase())) ||(_.entertainments[index].type.toLowerCase().contains(___.search.toLowerCase()))){
                  return EntertainmentItemWidget(entertainmentEntity: _.entertainments[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
