import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/restaurant/widgets/input_search_restaurant.widget.dart';
import 'package:ichillan_app/modules/restaurant/widgets/restaurant_list.widget.dart';

class RestaurantPage extends StatefulWidget {
  @override
  _RestaurantPageState createState() => _RestaurantPageState();
}

class _RestaurantPageState extends State<RestaurantPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Restaurant', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchRestaurantWidget(),
          Expanded(
            child:RestaurantListWidget(),
          )
        ],
      ),
    );
  }
}
