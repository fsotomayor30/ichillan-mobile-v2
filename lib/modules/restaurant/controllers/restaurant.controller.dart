import 'package:get/get.dart';
import 'package:ichillan_app/cloud_firestore/qualifies.function.dart';
import 'package:ichillan_app/entities/qualify.entity.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class RestaurantController extends GetxController{
  GlobalController _globalController = Get.find();
  QualifyFunction _qualifyFunction = QualifyFunction();
  String _search;
  String get search => _search;

  double _rating = 0.0;
  double get rating => _rating;

  void setRating(double rating){
    _rating = rating;
    update(['rating']);
  }

  void setSearch(String search){
    _search = search;
    update(['listRestaurant']);
  }


  double getSumQualifyByLocal(String local){
    double sum=0.0;

    List<QualifyEntity> qualifies = _globalController.qualifies.where((element) => element.idLocal == local).toList();
    qualifies.forEach((element) {
      sum=sum+double.parse(element.qualify);
    });

    return sum;
  }

  Future<bool> saveRating(String idLocal, String nombreLocal) async{
    bool validated = false;

    QualifyEntity qualifyEntity = QualifyEntity();
    var now = new DateTime.now();
    String fecha = now.day.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.year.toString();

    qualifyEntity.date = fecha;
    qualifyEntity.qualify = _rating.toString();
    qualifyEntity.name = nombreLocal;
    qualifyEntity.idLocal = idLocal;
    qualifyEntity.idUser = _globalController.userIMEI;

    List<QualifyEntity> ratingByUserAndLocal = _globalController.qualifies.where((element) => element.idUser == _globalController.userIMEI && element.idLocal == idLocal).toList();
    if(ratingByUserAndLocal.length == 0){
      await _qualifyFunction.saveRating(qualifyEntity);
      List<QualifyEntity> qualifyList = await _qualifyFunction.getAllQualifies();
      _globalController.setQualifyList(qualifyList);
      validated = true;
    }else{
      validated = false;
    }

    return validated;
  }

}