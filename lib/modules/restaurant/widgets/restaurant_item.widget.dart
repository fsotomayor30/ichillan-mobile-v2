import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/entities/restaurant.entity.dart';
import 'package:ichillan_app/modules/restaurant/controllers/restaurant.controller.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:url_launcher/url_launcher.dart';

class RestaurantItemWidget extends StatefulWidget {
  final RestaurantEntity restaurantEntity;

  RestaurantItemWidget({@required this.restaurantEntity});

  @override
  _RestaurantItemWidgetState createState() => _RestaurantItemWidgetState();
}

class _RestaurantItemWidgetState extends State<RestaurantItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: RestaurantController(),
        id: 'rating',
        builder: (__) => GetBuilder(
            init: GlobalController(),
            builder: (_) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: ListTile(
                    leading: Container(
                        width: 40,
                        height: 40,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(40.0),
                            child: CachedNetworkImage(
                              imageUrl: widget.restaurantEntity.image,
                              fit: BoxFit.fitWidth,
                              placeholder: (context, url) => Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                    child: new CircularProgressIndicator()),
                              ),
                              errorWidget: (context, url, error) =>
                                  new Icon(Icons.error),
                            ))),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                            child: Text(widget.restaurantEntity.name,
                                style: TextStyle(
                                    fontSize: 14, color: Color(0xFF707070)))),
                        SizedBox(width: 10),
                        Row(
                          children: [
                            FaIcon(FontAwesomeIcons.solidStar,
                                color: Colors.yellow, size: 15),
                            SizedBox(width: 2),
                            Text(
                                _.qualifies
                                            .where((element) =>
                                                element.idLocal ==
                                                widget.restaurantEntity.key)
                                            .length ==
                                        0
                                    ? "0"
                                    : (__.getSumQualifyByLocal(
                                                widget.restaurantEntity.key) /
                                            _.qualifies
                                                .where((element) =>
                                                    element.idLocal ==
                                                    widget.restaurantEntity.key)
                                                .length)
                                        .toStringAsFixed(0),
                                style: TextStyle(
                                    fontSize: 14, color: Color(0xFF707070))),
                          ],
                        )
                      ],
                    ),
                    subtitle: Text(
                      widget.restaurantEntity.type +
                          ", " +
                          widget.restaurantEntity.address,
                      style: TextStyle(fontSize: 10, color: Color(0xFF707070)),
                    ),
                    trailing: FaIcon(FontAwesomeIcons.ellipsisV,
                        size: 30, color: COLOR_PRIMARY),
                    onTap: () {
                      _sweetSheet.show(
                          context: context,
                          description: Text(
                            'Que quieres hacer?',
                            style: TextStyle(color: Color(0xff2D3748)),
                          ),
                          color: CustomSheetColor(
                            main: Colors.white,
                            accent: Colors.blue,
                            icon: Colors.blue,
                          ),
                          icon: FontAwesomeIcons.info,
                          actions: [
                            SweetSheetAction(
                              onPressed: () async {
                                if (await canLaunch(
                                    'https://www.google.com/maps/search/?api=1&query=' +
                                        widget.restaurantEntity.latitude
                                            .toString() +
                                        ',' +
                                        widget.restaurantEntity.longitude
                                            .toString())) {
                                  await launch(
                                      'https://www.google.com/maps/search/?api=1&query=' +
                                          widget.restaurantEntity.latitude
                                              .toString() +
                                          ',' +
                                          widget.restaurantEntity.longitude
                                              .toString());
                                } else {
                                  throw 'Could not launch ' +
                                      'https://www.google.com/maps/search/?api=1&query=' +
                                      widget.restaurantEntity.latitude
                                          .toString() +
                                      ',' +
                                      widget.restaurantEntity.longitude
                                          .toString();
                                }
                              },
                              title: 'IR',
                            ),
                            SweetSheetAction(
                              onPressed: () async {
                                await launch(
                                    'tel:' + widget.restaurantEntity.phone);
                              },
                              title: 'LLAMAR',
                            ),
                            SweetSheetAction(
                              onPressed: () async {
                                if (_.userIMEI == null) {
                                  Get.defaultDialog(
                                      buttonColor: COLOR_PRIMARY,
                                      confirmTextColor: Colors.white,
                                      onConfirm: () async {
                                        Get.back();
                                      },
                                      title: 'Error',
                                      content: Text(
                                          'No puedes calificar a este local'));
                                } else {
                                  __.setRating(0.0);
                                  Get.defaultDialog(
                                    buttonColor: COLOR_PRIMARY,
                                    confirmTextColor: Colors.white,
                                    onConfirm: () async {
                                      bool validated = await __.saveRating(
                                          widget.restaurantEntity.key,
                                          widget.restaurantEntity.name);
                                      Get.back();
                                      if (validated) {
                                        Get.defaultDialog(
                                            buttonColor: COLOR_PRIMARY,
                                            confirmTextColor: Colors.white,
                                            onConfirm: () async {
                                              Get.back();
                                            },
                                            title: 'Calificación',
                                            content: Text(
                                                'Has calificado con éxito'));
                                      } else {
                                        Get.defaultDialog(
                                            buttonColor: COLOR_PRIMARY,
                                            confirmTextColor: Colors.white,
                                            onConfirm: () async {
                                              Get.back();
                                            },
                                            title: 'Calificación',
                                            content: Text(
                                                'Ya has calificado a este local'));
                                      }
                                    },
                                    title: 'Calificación',
                                    content: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 10, 0, 10),
                                          child: Text(
                                              'Califica el servicio del local con estrellas de 1 a 5',
                                              style: TextStyle(
                                                  fontFamily:
                                                      "MontserratRegular")),
                                        ),
                                        SmoothStarRating(
                                            allowHalfRating: false,
                                            onRated: (value) {
                                              __.setRating(value);
                                            },
                                            starCount: 5,
                                            rating: __.rating,
                                            size: 40.0,
                                            filledIconData: Icons.star,
                                            halfFilledIconData: Icons.star_half,
                                            defaultIconData: Icons.star_border,
                                            color: Colors.blue,
                                            borderColor: Colors.lightBlue,
                                            spacing: 0.0),
                                      ],
                                    ),
                                  );
                                }
                              },
                              title: 'CALIFICAR',
                            ),
                          ]);
                    },
                  ),
                )));
  }
}
