import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/restaurant/controllers/restaurant.controller.dart';
import 'package:ichillan_app/modules/restaurant/widgets/restaurant_item.widget.dart';
import 'package:ichillan_app/modules/sport/controllers/sport.controller.dart';
import 'package:ichillan_app/modules/sport/widgets/sport_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class RestaurantListWidget extends StatefulWidget {
  @override
  _RestaurantListWidgetState createState() => _RestaurantListWidgetState();
}

class _RestaurantListWidgetState extends State<RestaurantListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listRestaurant',
            init: RestaurantController(),
            builder: (___) => ListView.builder(
              itemCount: _.restaurants.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.restaurants[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.restaurants[index].address.toLowerCase().contains(___.search.toLowerCase())) ||(_.restaurants[index].type.toLowerCase().contains(___.search.toLowerCase()))){
                  return RestaurantItemWidget(restaurantEntity: _.restaurants[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
