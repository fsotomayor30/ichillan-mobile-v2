import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/restaurant/controllers/restaurant.controller.dart';
import 'package:ichillan_app/modules/sport/controllers/sport.controller.dart';

class InputSearchRestaurantWidget extends StatefulWidget {
  @override
  _InputSearchRestaurantWidgetState createState() => _InputSearchRestaurantWidgetState();
}

class _InputSearchRestaurantWidgetState extends State<InputSearchRestaurantWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: RestaurantController(),
    builder: (_) => Padding(
      padding: const EdgeInsets.only(left: 15, top: 15, right: 15),
      child: TextField(
        onChanged: (String value){
          _.setSearch(value);
        },
        decoration: new InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.all(8),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(30.0),
            ),
          ),
          prefixIcon: const Icon(
            Icons.search,
          ),
          filled: false,
          hintStyle: new TextStyle(color: Colors.grey[800], fontSize: 12),
          hintText: "Buscar por nombre/dirección/tipo",
        ),
      ),
    ));
  }
}
