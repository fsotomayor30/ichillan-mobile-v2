import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/bottom_navigator/controllers/bottom_navigator.controller.dart';
import 'package:ichillan_app/modules/bottom_navigator/widgets/bottom_navigator.widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: BottomNavigatorController(),
        id: 'home',
        builder: (_) => Scaffold(
          body: _.contentHome,
          bottomNavigationBar: BottomNavigatorWidget(),
        )
    );
  }
}
