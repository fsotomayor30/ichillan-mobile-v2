import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/hotel/widgets/hotel_list.widget.dart';
import 'package:ichillan_app/modules/hotel/widgets/input_search_hotel.widget.dart';

class HotelPage extends StatefulWidget {
  @override
  _HotelPageState createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Hotelería', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchHotelWidget(),
          Expanded(
            child:HotelListWidget(),
          )
        ],
      ),
    );
  }
}
