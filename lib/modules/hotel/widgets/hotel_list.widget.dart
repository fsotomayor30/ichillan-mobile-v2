import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/hotel/controllers/hotel.controller.dart';
import 'package:ichillan_app/modules/hotel/widgets/hotel_item.widget.dart';
import 'package:ichillan_app/modules/sport/controllers/sport.controller.dart';
import 'package:ichillan_app/modules/sport/widgets/sport_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class HotelListWidget extends StatefulWidget {
  @override
  _HotelListWidgetState createState() => _HotelListWidgetState();
}

class _HotelListWidgetState extends State<HotelListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listHotel',
            init: HotelController(),
            builder: (___) => ListView.builder(
              itemCount: _.hotels.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.hotels[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.hotels[index].address.toLowerCase().contains(___.search.toLowerCase())) ||(_.hotels[index].type.toLowerCase().contains(___.search.toLowerCase()))){
                  return HotelItemWidget(hotelEntity: _.hotels[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
