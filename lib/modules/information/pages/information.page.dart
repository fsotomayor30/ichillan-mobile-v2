import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/filter_category.widget.dart';
import 'package:ichillan_app/modules/information/widgets/information_list.widget.dart';

class InformationPage extends StatefulWidget {
  @override
  _InformationPageState createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
        id: 'loadInformation',
    builder: (_) => Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Información', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: Container(),
      ),
      body: (!_.loadEmergencyPharmacy || !_.loadWeather || !_.loadGasoline93 || !_.loadGasoline95 || !_.loadGasoline97) ? Center(child: Container( child: CircularProgressIndicator(),)) : Column(
        children: [
          FilterCategoryWidget(),
          Expanded(child: InformationListWidget())
        ],
      ),
    ));
  }
}
