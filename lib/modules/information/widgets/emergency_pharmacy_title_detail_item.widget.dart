import 'package:flutter/material.dart';

class EmergencyPharmacyTitleDetailItemWidget extends StatefulWidget {

  @override
  _EmergencyPharmacyTitleDetailItemWidgetState createState() => _EmergencyPharmacyTitleDetailItemWidgetState();
}

class _EmergencyPharmacyTitleDetailItemWidgetState extends State<EmergencyPharmacyTitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 15),
        child: Text('Farmacia de turno', style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
