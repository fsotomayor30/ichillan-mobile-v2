import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_97_address_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_97_information_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_97_title_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/map_information.widget.dart';

class FuelStation97ItemWidget extends StatefulWidget {

  @override
  _FuelStation97ItemWidgetState createState() => _FuelStation97ItemWidgetState();
}

class _FuelStation97ItemWidgetState extends State<FuelStation97ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FuelStation97TitleDetailItemWidget(),
        FuelStation97InformationDetailItemWidget(),
        FuelStation97AddressDetailItemWidget(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: GetBuilder(
              init: InformationController(),
              builder: (_) =>  MapInformationWidget(zoom: 15, latitudeCenter: double.parse(_.fuelStation97Entity.ubicacion.latitud), longitudeCenter: double.parse(_.fuelStation97Entity.ubicacion.longitud))
          ),
        )
      ],
    );
  }
}
