import 'package:flutter/material.dart';
import 'package:ichillan_app/modules/information/widgets/weather_information_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/weather_title_detail_item.widget.dart';

class WeatherItemWidget extends StatefulWidget {

  @override
  _WeatherItemWidgetState createState() => _WeatherItemWidgetState();
}

class _WeatherItemWidgetState extends State<WeatherItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
       WeatherTitleDetailItemWidget(),
        WeatherInformationDetailItemWidget(),
      ],
    );
  }
}
