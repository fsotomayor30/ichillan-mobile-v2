import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';

class WeatherInformationDetailItemWidget extends StatefulWidget {

  @override
  _WeatherInformationDetailItemWidgetState createState() => _WeatherInformationDetailItemWidgetState();
}

class _WeatherInformationDetailItemWidgetState extends State<WeatherInformationDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
        builder: (_) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          width: 46,
                          height: 46,
                          child: CircleAvatar(
                            radius: 46,
                            backgroundImage: AssetImage(WEATHER_ICON),
                          ),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(_.weatherEntity.currentWeather.conditionWeather.text, style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w400)),
                                Text("Última actualización: "+_.weatherEntity.currentWeather.lastUpdate, style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300, fontSize: 8)),
                              ],
                            )),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(_.weatherEntity.currentWeather.temp.toStringAsFixed(0)+ "ºC", style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.bold, fontSize: 20)),
                ],
              ),
            ],
          ),
        ));
  }
}
