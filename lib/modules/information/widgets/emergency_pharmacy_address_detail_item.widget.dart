import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';

class EmergencyPharmacyAddressDetailItemWidget extends StatefulWidget {

  @override
  _EmergencyPharmacyAddressDetailItemWidgetState createState() => _EmergencyPharmacyAddressDetailItemWidgetState();
}

class _EmergencyPharmacyAddressDetailItemWidgetState extends State<EmergencyPharmacyAddressDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
    builder: (_) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Text(_.emergencyPharmacyEntity.direccion, textAlign: TextAlign.justify,style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300),),
    ));
  }
}
