import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class FuelStation95AddressDetailItemWidget extends StatefulWidget {

  @override
  _FuelStation95AddressDetailItemWidgetState createState() => _FuelStation95AddressDetailItemWidgetState();
}

class _FuelStation95AddressDetailItemWidgetState extends State<FuelStation95AddressDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
    builder: (_) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(_.fuelStation95Entity.direccion, textAlign: TextAlign.justify,style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300),),
          Row(
            children: [
              Text("\$ "+_.fuelStation95Entity.precios.precio95.toString(), textAlign: TextAlign.justify,style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300),),
              SizedBox(width: 5),
              FaIcon(FontAwesomeIcons.moneyBillAlt, color: COLOR_PRIMARY)
            ],
          ),
        ],
      ),
    ));
  }
}
