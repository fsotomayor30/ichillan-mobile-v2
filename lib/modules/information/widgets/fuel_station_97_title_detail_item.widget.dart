import 'package:flutter/material.dart';

class FuelStation97TitleDetailItemWidget extends StatefulWidget {

  @override
  _FuelStation97TitleDetailItemWidgetState createState() => _FuelStation97TitleDetailItemWidgetState();
}

class _FuelStation97TitleDetailItemWidgetState extends State<FuelStation97TitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 15),
        child: Text('Bencina 97 más barata', style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
