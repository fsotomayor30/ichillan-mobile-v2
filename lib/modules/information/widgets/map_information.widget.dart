import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:latlong2/latlong.dart';

class MapInformationWidget extends StatefulWidget {
  final double zoom;
  final double latitudeCenter;
  final double longitudeCenter;

  MapInformationWidget({
    @required this.zoom,
    @required this.latitudeCenter,
    @required this.longitudeCenter,
  });

  @override
  _MapInformationWidgetState createState() => _MapInformationWidgetState();
}

class _MapInformationWidgetState extends State<MapInformationWidget> {
  MapController _mapController = MapController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      child: FlutterMap(
        options: MapOptions(
          center: LatLng(widget.latitudeCenter, widget.longitudeCenter),
          zoom: widget.zoom,
        ),
        mapController: _mapController,
        layers: [
          //new TileLayerOptions(urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", subdomains: ['a', 'b', 'c']),
          new TileLayerOptions(
              urlTemplate:
                  "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']),
          new MarkerLayerOptions(markers: [
            Marker(
              width: 50,
              height: 50,
              point: new LatLng(widget.latitudeCenter, widget.longitudeCenter),
              builder: (ctx) =>
                  FaIcon(FontAwesomeIcons.mapMarker, color: COLOR_PRIMARY),
            )
          ]),
        ],
      ),
    );
  }
}
