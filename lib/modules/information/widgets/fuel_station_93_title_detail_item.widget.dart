import 'package:flutter/material.dart';

class FuelStation93TitleDetailItemWidget extends StatefulWidget {

  @override
  _FuelStation93TitleDetailItemWidgetState createState() => _FuelStation93TitleDetailItemWidgetState();
}

class _FuelStation93TitleDetailItemWidgetState extends State<FuelStation93TitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 15),
        child: Text('Bencina 93 más barata', style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
