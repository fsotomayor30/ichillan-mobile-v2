import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/emergency_pharmacy_address_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/emergency_pharmacy_information_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/emergency_pharmacy_title_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/map_information.widget.dart';

class EmergencyPharmacyItemWidget extends StatefulWidget {

  @override
  _EmergencyPharmacyItemWidgetState createState() => _EmergencyPharmacyItemWidgetState();
}

class _EmergencyPharmacyItemWidgetState extends State<EmergencyPharmacyItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        EmergencyPharmacyTitleDetailItemWidget(),
        EmergencyPharmacyInformationDetailItemWidget(),
        EmergencyPharmacyAddressDetailItemWidget(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: GetBuilder(
              init: InformationController(),
              builder: (_) => MapInformationWidget(zoom: 15, latitudeCenter: _.emergencyPharmacyEntity.latitud, longitudeCenter: _.emergencyPharmacyEntity.longitud)
          ),
        )
      ],
    );
  }
}
