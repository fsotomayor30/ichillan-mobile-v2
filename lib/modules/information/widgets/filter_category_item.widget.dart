import 'package:flutter/material.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class FilterCategoryItemWidget extends StatefulWidget {
  final String text;
  final bool selected;
  FilterCategoryItemWidget({@required this.text, @required this.selected});

  @override
  _FilterCategoryItemWidgetState createState() => _FilterCategoryItemWidgetState();
}

class _FilterCategoryItemWidgetState extends State<FilterCategoryItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: widget.selected ? COLOR_PRIMARY : Colors.transparent,
            border: Border.all(width: 1, color: widget.selected ? COLOR_PRIMARY : Color(0xFF707070),style: BorderStyle.solid)
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Text(widget.text, style: TextStyle(color: widget.selected ? Colors.white : Color(0xFF707070))),
        )
    );
  }
}
