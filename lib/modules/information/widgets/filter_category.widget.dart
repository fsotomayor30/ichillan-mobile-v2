import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/filter_category_item.widget.dart';

class FilterCategoryWidget extends StatefulWidget {
  @override
  _FilterCategoryWidgetState createState() => _FilterCategoryWidgetState();
}

class _FilterCategoryWidgetState extends State<FilterCategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
        id: 'filterInformation',
        builder: (_) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: (){
                        _.setSelectedAll(true);
                      },
                      child: FilterCategoryItemWidget(text:'Todas', selected: _.selectedAll)
                  ),
                  GestureDetector(
                      onTap: (){
                        _.setSelectedEmergencyPharmacy(true);
                      },
                      child: FilterCategoryItemWidget(text: 'Farmacia de turno', selected: _.selectedEmergencyPharmacy)
                  ),
                  GestureDetector(
                      onTap: (){
                        _.setSelectedWeather(true);
                      },
                      child: FilterCategoryItemWidget(text: 'Clima', selected: _.selectedWeather)
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: (){
                        _.setSelectedGasoline93(true);
                      },
                      child: FilterCategoryItemWidget(text: 'Bencina 93', selected: _.selectedGasoline93)
                  ),
                  GestureDetector(
                      onTap: (){
                        _.setSelectedGasoline95(true);
                      },
                      child: FilterCategoryItemWidget(text: 'Bencina 95', selected: _.selectedGasoline95)
                  ),
                  GestureDetector(
                      onTap: (){
                        _.setSelectedGasoline97(true);
                      },
                      child: FilterCategoryItemWidget(text: 'Bencina 97', selected: _.selectedGasoline97)
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
