import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class FuelStation93InformationDetailItemWidget extends StatefulWidget {

  @override
  _FuelStation93InformationDetailItemWidgetState createState() => _FuelStation93InformationDetailItemWidgetState();
}

class _FuelStation93InformationDetailItemWidgetState extends State<FuelStation93InformationDetailItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
        builder: (_) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          width: 46,
                          height: 46,
                          child: CircleAvatar(
                            radius: 46,
                            backgroundImage: AssetImage(GASOLINE_ICON),
                          ),
                        ),
                        SizedBox(width: 10),
                        Flexible(child: Text(_.fuelStation93Entity.distribuidor.nombre, style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w400))),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  GestureDetector(
                      onTap: (){
                        _sweetSheet.show(
                          context: context,
                          description: Text(
                            'Que quieres hacer?',
                            style: TextStyle(color: Color(0xff2D3748)),
                          ),
                          color: CustomSheetColor(
                            main: Colors.white,
                            accent: Colors.blue,
                            icon: Colors.blue,
                          ),
                          icon: FontAwesomeIcons.info,
                          actions:[
                            SweetSheetAction(
                              onPressed: () async {
                                if (await canLaunch(
                                    'https://www.google.com/maps/search/?api=1&query=' +
                                        _.fuelStation93Entity.ubicacion.latitud.toString() + ',' +
                                        _.fuelStation93Entity.ubicacion.longitud.toString())) {
                                  await launch(
                                      'https://www.google.com/maps/search/?api=1&query=' +
                                          _.fuelStation93Entity.ubicacion.latitud.toString() + ',' +
                                          _.fuelStation93Entity.ubicacion.longitud.toString());
                                } else {
                                  throw 'Could not launch ' +
                                      'https://www.google.com/maps/search/?api=1&query=' +
                                      _.fuelStation93Entity.ubicacion.latitud.toString() + ',' +
                                      _.fuelStation93Entity.ubicacion.longitud.toString();
                                }
                              },
                              title: 'IR',
                            )
                          ] ,
                        );
                      },
                      child: FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY)
                  ),
                ],
              ),

            ],
          ),
        ));
  }
}
