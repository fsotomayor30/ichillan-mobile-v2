import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_95_address_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_95_information_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_95_title_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/map_information.widget.dart';

class FuelStation95ItemWidget extends StatefulWidget {

  @override
  _FuelStation95ItemWidgetState createState() => _FuelStation95ItemWidgetState();
}

class _FuelStation95ItemWidgetState extends State<FuelStation95ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FuelStation95TitleDetailItemWidget(),
        FuelStation95InformationDetailItemWidget(),
        FuelStation95AddressDetailItemWidget(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: GetBuilder(
              init: InformationController(),
              builder: (_) =>  MapInformationWidget(zoom: 15, latitudeCenter: double.parse(_.fuelStation95Entity.ubicacion.latitud), longitudeCenter: double.parse(_.fuelStation95Entity.ubicacion.longitud))
          ),
        )
      ],
    );
  }
}
