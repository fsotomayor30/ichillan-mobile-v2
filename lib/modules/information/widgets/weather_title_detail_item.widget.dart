import 'package:flutter/material.dart';

class WeatherTitleDetailItemWidget extends StatefulWidget {

  @override
  _WeatherTitleDetailItemWidgetState createState() => _WeatherTitleDetailItemWidgetState();
}

class _WeatherTitleDetailItemWidgetState extends State<WeatherTitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 15),
        child: Text('Datos del clima', style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
