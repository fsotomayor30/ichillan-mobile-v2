import 'package:flutter/material.dart';

class FuelStation95TitleDetailItemWidget extends StatefulWidget {

  @override
  _FuelStation95TitleDetailItemWidgetState createState() => _FuelStation95TitleDetailItemWidgetState();
}

class _FuelStation95TitleDetailItemWidgetState extends State<FuelStation95TitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 15),
        child: Text('Bencina 95 más barata', style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
