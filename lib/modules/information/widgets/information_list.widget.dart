import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/emergency_pharmacy_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_93_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_95_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_97_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/weather_item.widget.dart';

class InformationListWidget extends StatefulWidget {
  @override
  _InformationListWidgetState createState() => _InformationListWidgetState();
}

class _InformationListWidgetState extends State<InformationListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: InformationController(),
    id: 'filterInformation',
    builder: (_) => ListView(
      children: [
        (_.selectedAll || _.selectedEmergencyPharmacy) ? EmergencyPharmacyItemWidget() : Container(),
        (_.selectedAll || _.selectedWeather) ? WeatherItemWidget() : Container(),
        (_.selectedAll || _.selectedGasoline93) ? FuelStation93ItemWidget() : Container(),
        (_.selectedAll || _.selectedGasoline95) ? FuelStation95ItemWidget() : Container(),
        (_.selectedAll || _.selectedGasoline97) ? FuelStation97ItemWidget() : Container(),

      ],
    ));
  }
}
