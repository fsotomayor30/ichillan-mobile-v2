import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/information/controllers/information.controller.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_93_address_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_93_information_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/fuel_station_93_title_detail_item.widget.dart';
import 'package:ichillan_app/modules/information/widgets/map_information.widget.dart';

class FuelStation93ItemWidget extends StatefulWidget {

  @override
  _FuelStation93ItemWidgetState createState() => _FuelStation93ItemWidgetState();
}

class _FuelStation93ItemWidgetState extends State<FuelStation93ItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FuelStation93TitleDetailItemWidget(),
        FuelStation93InformationDetailItemWidget(),
        FuelStation93AddressDetailItemWidget(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: GetBuilder(
              init: InformationController(),
              builder: (_) =>  MapInformationWidget(zoom: 15, latitudeCenter: double.parse(_.fuelStation93Entity.ubicacion.latitud), longitudeCenter: double.parse(_.fuelStation93Entity.ubicacion.longitud))
          ),
        )
      ],
    );
  }
}
