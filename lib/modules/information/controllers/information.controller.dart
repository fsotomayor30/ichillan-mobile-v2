import 'package:get/get.dart';
import 'package:ichillan_app/entities/emergency_pharmacy.entity.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';
import 'package:ichillan_app/entities/weather.entity.dart';
import 'package:ichillan_app/http/fuel_station.service.dart';
import 'package:ichillan_app/http/pharmacy.service.dart';
import 'package:ichillan_app/http/weather.service.dart';

class InformationController extends GetxController{
  PharmacyService _pharmacyService = PharmacyService();
  WeatherService _weatherService = WeatherService();
  FuelStationService _fuelStationService = FuelStationService();

  bool _selectedAll = true;
  bool get selectedAll => _selectedAll;

  bool _selectedEmergencyPharmacy = false;
  bool get selectedEmergencyPharmacy => _selectedEmergencyPharmacy;

  bool _selectedWeather = false;
  bool get selectedWeather => _selectedWeather;

  bool _selectedGasoline93 = false;
  bool get selectedGasoline93 => _selectedGasoline93;

  bool _selectedGasoline95 = false;
  bool get selectedGasoline95 => _selectedGasoline95;

  bool _selectedGasoline97 = false;
  bool get selectedGasoline97 => _selectedGasoline97;

  bool _loadEmergencyPharmacy = false;
  bool get loadEmergencyPharmacy => _loadEmergencyPharmacy;

  bool _loadWeather = false;
  bool get loadWeather => _loadWeather;

  bool _loadGasoline93 = false;
  bool get loadGasoline93 => _loadGasoline93;

  bool _loadGasoline95 = false;
  bool get loadGasoline95 => _loadGasoline95;

  bool _loadGasoline97 = false;
  bool get loadGasoline97 => _loadGasoline97;
  
  EmergencyPharmacyEntity _emergencyPharmacyEntity;
  EmergencyPharmacyEntity get emergencyPharmacyEntity => _emergencyPharmacyEntity;

  WeatherEntity _weatherEntity;
  WeatherEntity get weatherEntity => _weatherEntity;

  FuelStationEntity _fuelStation93Entity;
  FuelStationEntity get fuelStation93Entity => _fuelStation93Entity;

  FuelStationEntity _fuelStation95Entity;
  FuelStationEntity get fuelStation95Entity => _fuelStation95Entity;

  FuelStationEntity _fuelStation97Entity;
  FuelStationEntity get fuelStation97Entity => _fuelStation97Entity;

  void setEmergencyPharmacyEntity(EmergencyPharmacyEntity emergencyPharmacyEntity){
    _emergencyPharmacyEntity = emergencyPharmacyEntity;
  }

  void setWeatherEntity(WeatherEntity weatherEntity){
    _weatherEntity = weatherEntity;
  }

  void setFuelStation93Entity(FuelStationEntity fuelStation93Entity){
    _fuelStation93Entity = fuelStation93Entity;
  }

  void setFuelStation95Entity(FuelStationEntity fuelStation95Entity){
    _fuelStation95Entity = fuelStation95Entity;
  }

  void setFuelStation97Entity(FuelStationEntity fuelStation97Entity){
    _fuelStation97Entity = fuelStation97Entity;
  }

  void setLoadEmergencyPharmacy(bool loadEmergencyPharmacy){
    _loadEmergencyPharmacy = loadEmergencyPharmacy;
    update(['loadInformation']);
  }

  void setLoadWeather(bool loadWeather){
    _loadWeather = loadWeather;
    update(['loadInformation']);
  }

  void setLoadGasoline93(bool loadGasoline93){
    _loadGasoline93 = loadGasoline93;
    update(['loadInformation']);
  }

  void setLoadGasoline95(bool loadGasoline95){
    _loadGasoline95 = loadGasoline95;
    update(['loadInformation']);
  }

  void setLoadGasoline97(bool loadGasoline97){
    _loadGasoline97 = loadGasoline97;
    update(['loadInformation']);
  }

  void setSelectedAll(bool selectedAll){
    _selectedAll = selectedAll;
    _selectedEmergencyPharmacy = false;
    _selectedWeather = false;
    _selectedGasoline93 = false;
    _selectedGasoline95 = false;
    _selectedGasoline97 = false;
    update(['filterInformation']);
  }

  void setSelectedEmergencyPharmacy(bool selectedEmergencyPharmacy){
    _selectedEmergencyPharmacy = selectedEmergencyPharmacy;
    _selectedAll = false;
    _selectedWeather = false;
    _selectedGasoline93 = false;
    _selectedGasoline95 = false;
    _selectedGasoline97 = false;
    update(['filterInformation']);
  }

  void setSelectedWeather(bool selectedWeather){
    _selectedWeather = selectedWeather;
    _selectedAll = false;
    _selectedEmergencyPharmacy = false;
    _selectedGasoline93 = false;
    _selectedGasoline95 = false;
    _selectedGasoline97 = false;
    update(['filterInformation']);
  }

  void setSelectedGasoline93(bool selectedGasoline93){
    _selectedGasoline93 = selectedGasoline93;
    _selectedAll = false;
    _selectedEmergencyPharmacy = false;
    _selectedWeather = false;
    _selectedGasoline95 = false;
    _selectedGasoline97 = false;
    update(['filterInformation']);
  }

  void setSelectedGasoline95(bool selectedGasoline95){
    _selectedGasoline95 = selectedGasoline95;
    _selectedAll = false;
    _selectedEmergencyPharmacy = false;
    _selectedWeather = false;
    _selectedGasoline93 = false;
    _selectedGasoline97 = false;
    update(['filterInformation']);
  }

  void setSelectedGasoline97(bool selectedGasoline97){
    _selectedGasoline97 = selectedGasoline97;
    _selectedAll = false;
    _selectedEmergencyPharmacy = false;
    _selectedWeather = false;
    _selectedGasoline93 = false;
    _selectedGasoline95 = false;
    update(['filterInformation']);
  }
  
  Future<void> getEmergencyPharmacy() async{
    EmergencyPharmacyEntity emergencyPharmacyEntity = await _pharmacyService.getEmergencyPharmacy('CHILLAN');
    setEmergencyPharmacyEntity(emergencyPharmacyEntity);
    setLoadEmergencyPharmacy(true);

    WeatherEntity weatherEntity = await _weatherService.getWeather();
    setWeatherEntity(weatherEntity);
    setLoadWeather(true);

    FuelStationEntity fuelStation93Entity = await _fuelStationService.getFuelStation93();
    setFuelStation93Entity(fuelStation93Entity);
    setLoadGasoline93(true);

    FuelStationEntity fuelStation95Entity = await _fuelStationService.getFuelStation95();
    setFuelStation95Entity(fuelStation95Entity);
    setLoadGasoline95(true);

    FuelStationEntity fuelStation97Entity = await _fuelStationService.getFuelStation97();
    setFuelStation97Entity(fuelStation97Entity);
    setLoadGasoline97(true);
  }

  @override
  void onInit() {
    getEmergencyPharmacy();
    super.onInit();
  }
}