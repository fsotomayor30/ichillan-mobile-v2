import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/category/pages/category.page.dart';
import 'package:ichillan_app/modules/information/pages/information.page.dart';
import 'package:ichillan_app/modules/map/pages/map.page.dart';
import 'package:ichillan_app/modules/profile/pages/profile.page.dart';

class BottomNavigatorController extends GetxController{
  int _indexHome = 0;
  int get indexHome => _indexHome;

  Widget _contentHome = MapPage();
  Widget get contentHome => _contentHome;

  void setIndexHome(int indexHome){
    _indexHome = indexHome;
    switch(indexHome){
      case 0:
        setContentHome(MapPage());
        break;
      case 1:
        setContentHome(InformationPage());
        break;
      case 2:
        setContentHome(CategoryPage());
        break;
      case 3:
        setContentHome(ProfilePage());
        break;
    }
    update(['home', 'bottomNavigator']);
  }

  void setContentHome (Widget contentHome){
    _contentHome = contentHome;
    update(['home', 'bottomNavigator']);
  }
}