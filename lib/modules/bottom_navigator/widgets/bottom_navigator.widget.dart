import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/bottom_navigator/controllers/bottom_navigator.controller.dart';

class BottomNavigatorWidget extends StatefulWidget {
  @override
  _BottomNavigatorWidgetState createState() => _BottomNavigatorWidgetState();
}

class _BottomNavigatorWidgetState extends State<BottomNavigatorWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: BottomNavigatorController(),
        id: 'bottomNavigator',
        builder: (_) => BottomNavigationBar(
              onTap: (int index) {
                _.setIndexHome(index);
              },
              currentIndex: _.indexHome,
              items: getBottomNavigator(),
              type: BottomNavigationBarType.fixed,
            ));
  }

  List<BottomNavigationBarItem> getBottomNavigator() {
    List<BottomNavigationBarItem> bottomNavigators = [
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.mapMarkerAlt),
        label: '',
      ),
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.infoCircle),
        label: '',
      ),
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.thLarge),
        label: '',
      ),
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.userAlt),
        label: '',
      )
    ];

    return bottomNavigators;
  }
}
