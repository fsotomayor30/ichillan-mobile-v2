import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/bikes/controllers/bikes.controller.dart';
import 'package:latlong2/latlong.dart';

class MapBikesWidget extends StatefulWidget {
  @override
  _MapBikesWidgetState createState() => _MapBikesWidgetState();
}

class _MapBikesWidgetState extends State<MapBikesWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: BikesController(),
        builder: (_) => Container(
              height: Get.height,
              child: FlutterMap(
                options: MapOptions(
                  center: LatLng(-36.6067183, -72.1033265),
                  zoom: 13.0,
                ),
                layers: [
                  TileLayerOptions(
                      urlTemplate:
                          'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                      subdomains: ['a', 'b', 'c']),
                  PolylineLayerOptions(
                    polylines: _.bikesLanes,
                  ),
                ],
              ),
            ));
  }
}
