import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/cloud_firestore/bike_lanes.function.dart';
import 'package:ichillan_app/entities/bike_lanes.entity.dart';
import 'package:latlong2/latlong.dart';

class BikesController extends GetxController {
  BikesLanesFunction _bikesLanesFunction = BikesLanesFunction();

  bool _loadingBikesLanes = true;
  bool get loadingBikesLanes => _loadingBikesLanes;

  List<Polyline> _bikesLanes = [];
  List<Polyline> get bikesLanes => _bikesLanes;

  void setLoadingBikesLanes(bool loadingBikesLanes) {
    _loadingBikesLanes = loadingBikesLanes;
    update(['loadingBikesLanes']);
  }

  @override
  void onInit() async {
    List<BikesLanesEntity> likesLanes =
        await _bikesLanesFunction.getAllBikesLanes();
    likesLanes.forEach((e) {
      _bikesLanes.add(Polyline(
          points: [LatLng(e.point1, e.point2), LatLng(e.point3, e.point4)],
          strokeWidth: 4.0,
          color: Colors.blue));
    });
    setLoadingBikesLanes(false);
    super.onInit();
  }
}
