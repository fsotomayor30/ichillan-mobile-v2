import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/bikes/widgets/map_bikes.widget.dart';
import 'package:ichillan_app/modules/bikes/controllers/bikes.controller.dart';

class BikesPage extends StatefulWidget {
  @override
  _BikesPageState createState() => _BikesPageState();
}

class _BikesPageState extends State<BikesPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        id: 'loadingBikesLanes',
        init: BikesController(),
    builder: (_) => Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Ciclovias', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: _.loadingBikesLanes ? Center(child: CircularProgressIndicator()) : MapBikesWidget(),
    ));
  }
}