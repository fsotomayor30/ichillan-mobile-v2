import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/profile/pages/my_qualifications.page.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class MyQualificationsWidget extends StatefulWidget {
  @override
  _MyQualificationsWidgetState createState() => _MyQualificationsWidgetState();
}

class _MyQualificationsWidgetState extends State<MyQualificationsWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, top: 30, bottom: 30),
      child: GestureDetector(
        onTap: (){
          Get.to(() => MyQualificationsPage());
        },
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.transparent,
              border: Border.all(
                color: COLOR_PRIMARY,
                width: 1,
              )
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Mis calificaciones', style: TextStyle(fontSize: 20, color: Color(0xFF707070))),
              SizedBox(width: 10),
              FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow,)
            ],
          ),
        ),
      ),
    );
  }
}
