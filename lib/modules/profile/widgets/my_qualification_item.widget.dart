import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/qualify.entity.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';

class MyQualificationItemWidget extends StatefulWidget {
  final QualifyEntity qualifyEntity;

  MyQualificationItemWidget({@required this.qualifyEntity});

  @override
  _MyQualificationItemWidgetState createState() => _MyQualificationItemWidgetState();
}

class _MyQualificationItemWidgetState extends State<MyQualificationItemWidget> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Row(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(QUALIFY_ICON),
                ),
                SizedBox(width: 10),
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.qualifyEntity.name, style: TextStyle(fontSize: 14, color: Color(0xFF707070))),
                        SizedBox(height: 5),
                        Text(widget.qualifyEntity.date, style: TextStyle(fontSize: 10, color: Color(0xFF707070), fontWeight: FontWeight.w300)),
                      ],
                    )
                ),
              ],
            ),
          ),
          SizedBox(width: 20),
          Row(
            children: [
              FaIcon(FontAwesomeIcons.solidStar, color: Colors.yellow, size: 15),
              SizedBox(width: 2),
              Text(widget.qualifyEntity.qualify, style: TextStyle(fontSize: 14, color: Color(0xFF707070))),
            ],
          ),
        ],
      ),
    );
  }
}
