import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/profile/widgets/my_qualification_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class MyQualificationsListWidget extends StatefulWidget {
  @override
  _MyQualificationsListWidgetState createState() => _MyQualificationsListWidgetState();
}

class _MyQualificationsListWidgetState extends State<MyQualificationsListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => ListView.builder(
          itemCount: _.qualifies.length,
          shrinkWrap: false,
          padding: EdgeInsets.symmetric(vertical: 15),
          itemBuilder: (__, index){
            if(_.qualifies[index].idUser == _.userIMEI){
              return MyQualificationItemWidget(qualifyEntity: _.qualifies[index]);
            }else{
              return Container();
            }
          },
        )
    );
  }
}
