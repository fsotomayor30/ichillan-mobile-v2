import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class InfoUserWidget extends StatefulWidget {
  @override
  _InfoUserWidgetState createState() => _InfoUserWidgetState();
}

class _InfoUserWidgetState extends State<InfoUserWidget> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 70,
      left: 0,
      right: 0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Container(
          height: 216,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 42,
                backgroundColor: COLOR_PRIMARY,
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(SPLASH_ICON),
                ),
              ),
              SizedBox(height: 10),
              GetBuilder(
                  init: GlobalController(),
                  builder: (_) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Text("Identificador: "+(_.userIMEI == null ? '-' : _.userIMEI), textAlign: TextAlign.center ,style: TextStyle(fontSize: 15, color: Color(0xFF707070))),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
