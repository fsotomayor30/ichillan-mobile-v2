import 'package:flutter/material.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class BackgroundTopWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: Container(
        height: 250,
        decoration: BoxDecoration(
          color: COLOR_PRIMARY,
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(40),
              bottomLeft: Radius.circular(40)),
        ),
      ),
    );
  }
}
