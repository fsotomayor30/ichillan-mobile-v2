import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class QualifiesUserWidget extends StatefulWidget {
  @override
  _QualifiesUserWidgetState createState() => _QualifiesUserWidgetState();
}

class _QualifiesUserWidgetState extends State<QualifiesUserWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) =>  Center(
          child: Container(
            height: 120,
            width: 120,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(120),
                color: Colors.transparent,
                border: Border.all(
                  color: COLOR_PRIMARY,
                  width: 1,
                )
            ),
            child: Center(
              child: Column(
                children: [
                  Text(_.qualifies.where((element) => element.idUser == _.userIMEI).length.toString(),  maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 80, color: Color(0xFF58B0F6)),),
                  Text('Calificaciones', style: TextStyle(fontSize: 8, color: Color(0xFF58B0F6)),),
                ],
              ),
            ),
          ),
        ));
  }
}
