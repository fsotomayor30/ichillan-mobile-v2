import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/profile/controllers/profile.controller.dart';
import 'package:ichillan_app/modules/profile/widgets/my_qualifications_list.widget.dart';

class MyQualificationsPage extends StatefulWidget {
  @override
  _MyQualificationsPageState createState() => _MyQualificationsPageState();
}

class _MyQualificationsPageState extends State<MyQualificationsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Mis calificaciones', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: MyQualificationsListWidget(),
    );
  }
}
