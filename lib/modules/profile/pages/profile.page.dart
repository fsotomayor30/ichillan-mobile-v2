import 'package:flutter/material.dart';
import 'package:ichillan_app/modules/profile/widgets/background_top.widget.dart';
import 'package:ichillan_app/modules/profile/widgets/info_user.widget.dart';
import 'package:ichillan_app/modules/profile/widgets/my_qualifications.widget.dart';
import 'package:ichillan_app/modules/profile/widgets/qualifies_user.widget.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                BackgroundTopWidget(),
                InfoUserWidget(),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                QualifiesUserWidget(),
                MyQualificationsWidget()
              ],
            ),
          )
        ],
      ),
    );
  }
}
