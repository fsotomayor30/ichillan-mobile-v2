import 'package:get/get.dart';

class MapController extends GetxController{
  bool _showFilter = true;
  bool get showFilter => _showFilter;

  bool _showEntrepreneurshipFilter = false;
  bool get showEntrepreneurshipFilter => _showEntrepreneurshipFilter;

  bool _showAll = true;
  bool get showAll => _showAll;

  bool _showSport = false;
  bool get showSport => _showSport;

  bool _showEntertainment = false;
  bool get showEntertainment => _showEntertainment;

  bool _showCulture = false;
  bool get showCulture => _showCulture;

  bool _showPark = false;
  bool get showPark => _showPark;

  bool _showHotel = false;
  bool get showHotel => _showHotel;

  bool _showRestaurant = false;
  bool get showRestaurant => _showRestaurant;

  bool _showEducation = false;
  bool get showEducation => _showEducation;

  bool _showSupermarket = false;
  bool get showSupermarket => _showSupermarket;

  bool _showVeterinary = false;
  bool get showVeterinary => _showVeterinary;

  bool _showFuelStation = false;
  bool get showFuelStation => _showFuelStation;

  bool _showPharmacy = false;
  bool get showPharmacy => _showPharmacy;


  void setShowFilter(bool showFilter){
    _showFilter = showFilter;
    update(['filter']);
  }

  void setShowEntrepreneurshipFilter(bool showEntrepreneurshipFilter){
    _showEntrepreneurshipFilter = showEntrepreneurshipFilter;
    update(['entrepreneurshipFilter']);
  }

  void setShowAll(bool showAll){
    _showAll = showAll;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant = false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary = false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowSport(bool showSport){
    _showSport = showSport;
    _showAll = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowEntertainment(bool showEntertainment){
    _showEntertainment = showEntertainment;
    _showSport = false;
    _showAll = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowCulture(bool showCulture){
    _showCulture = showCulture;
    _showSport = false;
    _showEntertainment = false;
    _showAll = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowPark(bool showPark){
    _showPark = showPark;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showAll = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowHotel(bool showHotel){
    _showHotel = showHotel;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showAll = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowRestaurant(bool showRestaurant){
    _showRestaurant = showRestaurant;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showAll = false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowEducation(bool showEducation){
    _showEducation = showEducation;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showAll = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowSupermarket(bool showSupermarket){
    _showSupermarket = showSupermarket;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showAll = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowVeterinary(bool showVeterinary){
    _showVeterinary = showVeterinary;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showAll = false;
    _showFuelStation = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowFuelStation(bool showFuelStation){
    _showFuelStation = showFuelStation;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showAll = false;
    _showPharmacy = false;
    update(['markers', 'filter']);
  }

  void setShowPharmacy(bool showPharmacy){
    _showPharmacy = showPharmacy;
    _showSport = false;
    _showEntertainment = false;
    _showCulture = false;
    _showPark = false;
    _showHotel = false;
    _showRestaurant= false;
    _showEducation = false;
    _showSupermarket = false;
    _showVeterinary= false;
    _showFuelStation = false;
    _showAll = false;
    update(['markers', 'filter']);
  }
}