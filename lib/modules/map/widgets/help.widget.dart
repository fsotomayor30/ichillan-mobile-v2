
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/help/pages/help.page.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class HelpWidget extends StatefulWidget {
  @override
  _HelpWidgetState createState() => _HelpWidgetState();
}

class _HelpWidgetState extends State<HelpWidget> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 220,
        right: 0,
        child: GestureDetector(
          onTap: (){
            Get.to(() => HelpPage());
          },
          child: Container(
            width: 30,
            height: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), topLeft: Radius.circular(10)),
              color: Colors.white,
            ),
            child: RotatedBox(
              quarterTurns: 3,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FaIcon(FontAwesomeIcons.exclamationTriangle, size: 15, color: COLOR_PRIMARY,),
                    SizedBox(width: 5),
                    Text('Ayuda', style: TextStyle(color: COLOR_PRIMARY)),
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }
}
