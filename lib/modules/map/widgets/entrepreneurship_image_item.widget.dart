import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/modules/entrepreneurship/pages/entrepreneurship_detail.page.dart';

class EntrepreneurshipImageItemWidget extends StatefulWidget {

  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipImageItemWidget({@required this.entrepreneurshipEntity});
  @override
  _EntrepreneurshipImageItemWidgetState createState() => _EntrepreneurshipImageItemWidgetState();
}

class _EntrepreneurshipImageItemWidgetState extends State<EntrepreneurshipImageItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(() => EntrepreneurshipDetailPage(entrepreneurshipEntity: widget.entrepreneurshipEntity));
      },
      child: CachedNetworkImage(
        imageUrl: widget.entrepreneurshipEntity.photoPrincipal,
        placeholder: (context, url) => Padding(
          padding: const EdgeInsets.all(10.0),
          child: new CircularProgressIndicator(),
        ),
        errorWidget: (context, url, error) => new Icon(Icons.error),
      ),
    );
  }
}
