import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/map/controllers/map.controller.dart';
import 'package:ichillan_app/modules/map/widgets/entrepreneurship_image_item.widget.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class EntrepreneurshipWidget extends StatefulWidget {
  @override
  _EntrepreneurshipWidgetState createState() => _EntrepreneurshipWidgetState();
}

class _EntrepreneurshipWidgetState extends State<EntrepreneurshipWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: MapController(),
        id: 'entrepreneurshipFilter',
        builder: (_) => _.showEntrepreneurshipFilter
            ? Positioned(
                bottom: 350,
                left: 10,
                right: 0,
                child: Container(
                  width: 30,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        topLeft: Radius.circular(10)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: 5),
                      GestureDetector(
                        onTap: () {
                          _.setShowEntrepreneurshipFilter(false);
                        },
                        child: RotatedBox(
                          quarterTurns: 3,
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FaIcon(
                                  FontAwesomeIcons.times,
                                  size: 15,
                                  color: COLOR_PRIMARY,
                                ),
                                SizedBox(width: 5),
                                Text('Emprendimientos',
                                    style: TextStyle(color: COLOR_PRIMARY)),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: Get.width - 5 - 14 - 10 - 2,
                        height: 150,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: GetBuilder(
                              init: GlobalController(),
                              builder: (__) => GridView.builder(
                                    itemCount: __.entrepreneurships.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2),
                                    scrollDirection: Axis.horizontal,
                                    padding: EdgeInsets.all(0),
                                    itemBuilder: (___, index) {
                                      return EntrepreneurshipImageItemWidget(
                                          entrepreneurshipEntity:
                                              __.entrepreneurships[index]);
                                    },
                                  )),
                        ),
                      ),
                    ],
                  ),
                ))
            : Positioned(
                bottom: 350,
                right: 0,
                child: GestureDetector(
                  onTap: () {
                    _.setShowEntrepreneurshipFilter(true);
                  },
                  child: Container(
                    width: 30,
                    height: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          topLeft: Radius.circular(10)),
                      color: Colors.white,
                    ),
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FaIcon(
                              FontAwesomeIcons.rocket,
                              size: 15,
                              color: COLOR_PRIMARY,
                            ),
                            SizedBox(width: 5),
                            Text('Emprendimientos',
                                style: TextStyle(color: COLOR_PRIMARY)),
                          ],
                        ),
                      ),
                    ),
                  ),
                )));
  }
}
