
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/entities/culture.entity.dart';
import 'package:ichillan_app/entities/education.entity.dart';
import 'package:ichillan_app/entities/entertainment.entity.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';
import 'package:ichillan_app/entities/hotel.entity.dart';
import 'package:ichillan_app/entities/park.entity.dart';
import 'package:ichillan_app/entities/pharmacy.entity.dart';
import 'package:ichillan_app/entities/restaurant.entity.dart';
import 'package:ichillan_app/entities/sport.entity.dart';
import 'package:ichillan_app/entities/supermarket.entity.dart';
import 'package:ichillan_app/entities/veterinary.entity.dart';
import 'package:ichillan_app/modules/map/controllers/map.controller.dart' as MapsController;
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';
import 'package:latlong2/latlong.dart';

class MapWidget extends StatefulWidget {
  final double zoom;
  final double latitudeCenter;
  final double longitudeCenter;
  final List<ParkEntity> parkMarkers;
  final List<SportEntity> sportMarkers;
  final List<EducationEntity> educationMarkers;
  final List<EntertainmentEntity> entertainmentMarkers;
  final List<HotelEntity> hotelMarkers;
  final List<RestaurantEntity> restaurantMarkers;
  final List<CultureEntity> cultureMarkers;
  final List<SupermarketEntity> supermarketMarkers;
  final List<VeterinaryEntity> veterinaryMarkers;
  final List<FuelStationEntity> fuelStationMarkers;
  final List<PharmacyEntity> pharmacyMarkers;


  MapWidget({@required this.zoom,
    @required this.latitudeCenter,
    @required this.longitudeCenter,
    @required this.parkMarkers,
    @required this.sportMarkers,
    @required this.educationMarkers,
    @required this.entertainmentMarkers,
    @required this.hotelMarkers,
    @required this.restaurantMarkers,
    @required this.cultureMarkers,
    @required this.supermarketMarkers,
    @required this.veterinaryMarkers,
    @required this.fuelStationMarkers,
    @required this.pharmacyMarkers
  });

  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  MapController _mapController = MapController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        id: 'map',
        builder: (_) =>
            Container(
              //height: 100,
              child: GetBuilder(
                  init: MapsController.MapController(),
                  id: 'markers',
                  builder: (__) => FlutterMap(
                    options: MapOptions(
                      center: _.usersCenter,
                      zoom: widget.zoom,
                    ),
                    mapController: _mapController,
                    layers: [
                      //new TileLayerOptions(urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", subdomains: ['a', 'b', 'c']),
                      new TileLayerOptions(urlTemplate: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png", subdomains: ['a', 'b', 'c']),
                      new MarkerLayerOptions(
                          markers: __.showAll || __.showPark ? getParkMarkers() : []
                            ..addAll(__.showAll || __.showSport ? getSportMarkers() : []
                              ..addAll(__.showAll || __.showEducation ? getEducationMarkers() : []
                                ..addAll(__.showAll || __.showEntertainment ? getEntertainmentMarkers() : []
                                  ..addAll(__.showAll || __.showHotel ? getHotelMarkers() : []
                                    ..addAll(__.showAll || __.showRestaurant ? getRestaurantMarkers() : []
                                      ..addAll(__.showAll || __.showCulture ? getCultureMarkers() : []
                                        ..addAll(__.showAll || __.showSupermarket ? getSupermarketMarkers() : []
                                          ..addAll(__.showAll || __.showVeterinary ? getVeterinaryMarkers() : []
                                            ..addAll(__.showAll || __.showFuelStation ? getFuelStationMarkers() : []
                                              ..addAll(__.showAll || __.showPharmacy ? getPharmacyMarkers() : [])))))
                                    )))))
                      ),
                    ],
                  )),
            ));
  }

  List<Marker> getParkMarkers() {
    List<Marker> markers = List<Marker>();
    widget.parkMarkers.forEach((marker) {
      markers.add(Marker(
        width: 50,
        height: 50,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Text(marker.address)
                );
              },
              child: new Container(
                child: Image.asset(TREE_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getSportMarkers() {
    List<Marker> markers = List<Marker>();
    widget.sportMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Text(marker.address + '\n' + marker.type)
                );
              },
              child: new Container(
                child: Image.asset(SPORT_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getEducationMarkers() {
    List<Marker> markers = List<Marker>();
    widget.educationMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Text(marker.address + '\n' + marker.type)
                );
              },
              child: new Container(
                child: Image.asset(EDUCATION_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getEntertainmentMarkers() {
    List<Marker> markers = List<Marker>();
    widget.entertainmentMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Row(
                      children: [
                        Image.network(marker.image, width: 50),
                        SizedBox(width: 10,),
                        Flexible(child: Text(marker.address + '\n' + marker.type)),
                      ],
                    )
                );
              },
              child: new Container(
                child: Image.network(marker.image),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getRestaurantMarkers() {
    List<Marker> markers = List<Marker>();
    widget.restaurantMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Row(
                      children: [
                        Image.network(marker.image, width: 50),
                        SizedBox(width: 10,),
                        Flexible(child: Text(marker.address + '\n' + marker.type)),
                      ],
                    )
                );
              },
              child: new Container(
                child: Image.network(marker.image),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getHotelMarkers() {
    List<Marker> markers = List<Marker>();
    widget.hotelMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Flexible(child: Text(marker.address + '\n' + marker.type))
                );
              },
              child: new Container(
                child: Image.asset(HOTEL_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getSupermarketMarkers() {
    List<Marker> markers = List<Marker>();
    widget.supermarketMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Row(
                      children: [
                        Image.network(marker.image, width: 50),
                        SizedBox(width: 10,),
                        Flexible(child: Text(marker.address)),
                      ],
                    )
                );
              },
              child: new Container(
                child: Image.network(marker.image),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getCultureMarkers() {
    List<Marker> markers = List<Marker>();
    widget.cultureMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Text('')
                );
              },
              child: new Container(
                child: Image.asset(CULTURE_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getVeterinaryMarkers() {
    List<Marker> markers = List<Marker>();
    widget.veterinaryMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitude, marker.longitude),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.name,
                    content: Flexible(child: Text(marker.address))
                );
              },
              child: new Container(
                child: Image.asset(VETERINARY_ICON),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getFuelStationMarkers() {
    List<Marker> markers = List<Marker>();
    widget.fuelStationMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(double.parse(marker.ubicacion.latitud), double.parse(marker.ubicacion.longitud)),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.distribuidor.nombre,
                    content: Row(
                      children: [
                        Image.network(marker.distribuidor.logo, width: 50),
                        SizedBox(width: 10,),
                        Flexible(child: Text(marker.direccion)),
                      ],
                    )
                );
              },
              child: new Container(
                child: Image.network(marker.distribuidor.logo),
              ),
            ),
      ));
    });

    return markers;
  }

  List<Marker> getPharmacyMarkers() {
    List<Marker> markers = List<Marker>();
    widget.pharmacyMarkers.forEach((marker) {
      markers.add(Marker(
        width: 30,
        height: 30,
        point: new LatLng(marker.latitud, marker.longitud),
        builder: (ctx) =>
            GestureDetector(
              onTap: () {
                Get.defaultDialog(
                    buttonColor: COLOR_PRIMARY,
                    confirmTextColor: Colors.white,
                    onConfirm: () => Get.back(),
                    title: marker.nombre,
                    content: Flexible(child: Text(marker.direccion))
                );
              },
              child: new Container(
                child: Image.asset(PHARMACY_ICON),
              ),
            ),
      ));
    });

    return markers;
  }
}
