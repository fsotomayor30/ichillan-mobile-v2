import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/map/controllers/map.controller.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class FilterWidget extends StatefulWidget {
  @override
  _FilterWidgetState createState() => _FilterWidgetState();
}

class _FilterWidgetState extends State<FilterWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: MapController(),
        id: 'filter',
        builder: (_) => _.showFilter ?
        Positioned(
            left: 10,
            right: 0,
            bottom: 90,
            child: Container(
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), topLeft: Radius.circular(10)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                children: [
                  SizedBox(width: 5),
                  GestureDetector(
                    onTap: (){
                      _.setShowFilter(false);
                    },
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FaIcon(FontAwesomeIcons.times, size: 15, color: COLOR_PRIMARY),
                            SizedBox(width: 5),
                            Text('Filtrar', style: TextStyle(color: COLOR_PRIMARY, fontSize: 14)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: Get.width - 5 - 14 - 10 - 2,
                    height: 89,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              _.setShowAll(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showAll ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.bars, color: Colors.white, size: 44),
                                          Text('Todas', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowSport(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showSport ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.running, color: Colors.white, size: 44),
                                          Text('Deportes', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowEntertainment(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showEntertainment ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.cocktail, color: Colors.white, size: 44),
                                          Text('Entretenimiento',textAlign: TextAlign.center,  style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowCulture(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showCulture ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.cameraRetro, color: Colors.white, size: 44),
                                          Text('Ruta Cultural', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowPark(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showPark ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.tree, color: Colors.white, size: 44),
                                          Text('Plazas', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowHotel(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showHotel ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.hSquare, color: Colors.white, size: 44),
                                          Text('Hotelería', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowFuelStation(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showFuelStation ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.gasPump, color: Colors.white, size: 44),
                                          Text('Estaciones de bencina', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowRestaurant(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showRestaurant ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.utensils, color: Colors.white, size: 44),
                                          Text('Restaurantes', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowEducation(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showEducation ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.school, color: Colors.white, size: 44),
                                          Text('Educación', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowPharmacy(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showPharmacy ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.capsules, color: Colors.white, size: 44),
                                          Text('Farmacias', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowSupermarket(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showSupermarket ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.shoppingBasket, color: Colors.white, size: 44),
                                          Text('Supermercados', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              _.setShowVeterinary(true);
                            },
                            child: Container(
                                width: 96.0,
                                height: 96,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue,
                                ),
                                child: Stack(
                                  children: [
                                    _.showVeterinary ? Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          color: Colors.white,
                                        ),
                                        child: Center(
                                          child: FaIcon(FontAwesomeIcons.check, color: Colors.green, size: 15),
                                        ),
                                      ),
                                    ) : Container(),
                                    Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          FaIcon(FontAwesomeIcons.cat, color: Colors.white, size: 44),
                                          Text('Veterinarias', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(width: 15),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ))
            : Positioned(
            bottom: 90,
            right: 0,
            child: GestureDetector(
              onTap: (){
                _.setShowFilter(true);
              },
              child: Container(
                width: 30,
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), topLeft: Radius.circular(10)),
                  color: Colors.white,

                ),
                child: RotatedBox(
                  quarterTurns: 3,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FaIcon(FontAwesomeIcons.bars, size: 15, color: COLOR_PRIMARY),
                        SizedBox(width: 5),
                        Text('Filtrar', style: TextStyle(color: COLOR_PRIMARY)),
                      ],
                    ),
                  ),
                ),
              ),
            )
        ));
  }
}
