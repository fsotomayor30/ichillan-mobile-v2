import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/bikes/pages/bikes.page.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class BikeWidget extends StatefulWidget {
  @override
  _BikeWidgetState createState() => _BikeWidgetState();
}

class _BikeWidgetState extends State<BikeWidget> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 220,
        left: 0,
        child: GestureDetector(
          onTap: () async {
            Get.to(() => BikesPage());
          },
          child: Container(
            width: 30,
            height: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(10), topRight: Radius.circular(10)),
              color: Colors.white,
            ),
            child: RotatedBox(
              quarterTurns: 1,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FaIcon(FontAwesomeIcons.bicycle, size: 15, color: COLOR_PRIMARY,),
                    SizedBox(width: 5),
                    Text('Ciclovías', style: TextStyle(color: COLOR_PRIMARY)),
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }
}
