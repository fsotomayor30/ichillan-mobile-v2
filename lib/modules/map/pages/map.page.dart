import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/map/controllers/map.controller.dart';
import 'package:ichillan_app/modules/map/widgets/banner.widget.dart';
import 'package:ichillan_app/modules/map/widgets/bike.widget.dart';
import 'package:ichillan_app/modules/map/widgets/entrepreneurship.widget.dart';
import 'package:ichillan_app/modules/map/widgets/filter.widget.dart';
import 'package:ichillan_app/modules/map/widgets/help.widget.dart';
import 'package:ichillan_app/modules/map/widgets/map.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: MapController(),
        builder: (_) =>Stack(
          children: [
            GetBuilder(
                init: GlobalController(),
                builder: (__) => Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: MapWidget(
                      zoom: 15,
                      latitudeCenter: __.usersCenter.latitude,
                      longitudeCenter: __.usersCenter.longitude,
                      parkMarkers: __.parks,
                      sportMarkers: __.sports,
                      educationMarkers: __.educations,
                      entertainmentMarkers: __.entertainments,
                      hotelMarkers: __.hotels,
                      restaurantMarkers: __.restaurants,
                      cultureMarkers: __.cultures,
                      supermarketMarkers: __.supermarkets,
                      veterinaryMarkers: __.vets,
                      fuelStationMarkers: __.fuelStations,
                      pharmacyMarkers: __.pharmacies,
                    )
                )),
            BannerWidget(),
            EntrepreneurshipWidget(),
            HelpWidget(),
            FilterWidget(),
            BikeWidget(),
          ],
        ));
  }
}
