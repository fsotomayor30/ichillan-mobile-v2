import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class FuelStationItemWidget extends StatefulWidget {
  final FuelStationEntity fuelStationEntity;

  FuelStationItemWidget({@required this.fuelStationEntity});

  @override
  _FuelStationItemWidgetState createState() => _FuelStationItemWidgetState();
}

class _FuelStationItemWidgetState extends State<FuelStationItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        leading: Container(
            width: 40,
            height: 40,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: CachedNetworkImage(
                  imageUrl: widget.fuelStationEntity.distribuidor.logo, fit: BoxFit.fitWidth,
                  placeholder: (context, url) => Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(child: new CircularProgressIndicator()),
                  ),
                  errorWidget: (context, url, error) => new Icon(Icons.error),
                )
            )
        ),
        title: Text(widget.fuelStationEntity.distribuidor.nombre, style: TextStyle(fontSize: 14, color: Color(0xFF707070)),),
        subtitle: Text(widget.fuelStationEntity.direccion, style: TextStyle(fontSize: 10, color: Color(0xFF707070)),),
        trailing: FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY),
        onTap: (){
          _sweetSheet.show(
              context: context,
              description: Text(
                'Que quieres hacer?',
                style: TextStyle(color: Color(0xff2D3748)),
              ),
              color: CustomSheetColor(
                main: Colors.white,
                accent: Colors.blue,
                icon: Colors.blue,
              ),
              icon: FontAwesomeIcons.info,
              actions:
              [SweetSheetAction(
                onPressed: () async {
                  if (await canLaunch(
                      'https://www.google.com/maps/search/?api=1&query=' +
                          widget.fuelStationEntity.ubicacion.latitud.toString() + ',' +
                          widget.fuelStationEntity.ubicacion.longitud.toString())) {
                    await launch(
                        'https://www.google.com/maps/search/?api=1&query=' +
                            widget.fuelStationEntity.ubicacion.latitud.toString() + ',' +
                            widget.fuelStationEntity.ubicacion.longitud.toString());
                  } else {
                    throw 'Could not launch ' +
                        'https://www.google.com/maps/search/?api=1&query=' +
                        widget.fuelStationEntity.ubicacion.latitud.toString() + ',' +
                        widget.fuelStationEntity.ubicacion.longitud.toString();
                  }
                },
                title: 'IR',
              )
              ]
          );
        },
      ),
    );
  }
}
