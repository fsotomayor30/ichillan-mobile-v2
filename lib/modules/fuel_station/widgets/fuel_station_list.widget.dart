import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/fuel_station/controllers/fuel_station.controller.dart';
import 'package:ichillan_app/modules/fuel_station/widgets/fuel_station_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class FuelStationListWidget extends StatefulWidget {
  @override
  _FuelStationListWidgetState createState() => _FuelStationListWidgetState();
}

class _FuelStationListWidgetState extends State<FuelStationListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listFuelStation',
            init: FuelStationController(),
            builder: (___) => ListView.builder(
              itemCount: _.fuelStations.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.fuelStations[index].distribuidor.nombre.toLowerCase().contains(___.search.toLowerCase())) ||(_.fuelStations[index].direccion.toLowerCase().contains(___.search.toLowerCase()))){
                  return FuelStationItemWidget(fuelStationEntity: _.fuelStations[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
