import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/fuel_station/widgets/fuel_station_list.widget.dart';
import 'package:ichillan_app/modules/fuel_station/widgets/input_search_fuel_station.widget.dart';

class FuelStationPage extends StatefulWidget {
  @override
  _FuelStationPageState createState() => _FuelStationPageState();
}

class _FuelStationPageState extends State<FuelStationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Estaciones de bencina', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchFuelStationWidget(),
          Expanded(
            child: FuelStationListWidget(),
          )
        ],
      ),
    );
  }
}
