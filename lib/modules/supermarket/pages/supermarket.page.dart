import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/supermarket/widgets/input_search_supermarket.widget.dart';
import 'package:ichillan_app/modules/supermarket/widgets/supermarket_list.widget.dart';

class SupermarketPage extends StatefulWidget {
  @override
  _SupermarketPageState createState() => _SupermarketPageState();
}

class _SupermarketPageState extends State<SupermarketPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Supermercados', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchSupermarketWidget(),
          Expanded(
            child:SupermarketListWidget(),
          )
        ],
      ),
    );
  }
}
