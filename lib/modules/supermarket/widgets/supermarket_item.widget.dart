import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/supermarket.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class SupermarketItemWidget extends StatefulWidget {
  final SupermarketEntity supermarketEntity;

  SupermarketItemWidget({@required this.supermarketEntity});

  @override
  _SupermarketItemWidgetState createState() => _SupermarketItemWidgetState();
}

class _SupermarketItemWidgetState extends State<SupermarketItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        leading: Container(
          width: 40,
          height: 40,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(40.0),
              child: CachedNetworkImage(
                imageUrl: widget.supermarketEntity.image,
                fit: BoxFit.fitWidth,
                placeholder: (context, url) => Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(child: new CircularProgressIndicator()),
                ),
                errorWidget: (context, url, error) => new Icon(Icons.error),
              )),
        ),
        title: Text(
          widget.supermarketEntity.name,
          style: TextStyle(fontSize: 14, color: Color(0xFF707070)),
        ),
        subtitle: Text(
          widget.supermarketEntity.address,
          style: TextStyle(fontSize: 10, color: Color(0xFF707070)),
        ),
        trailing:
            FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY),
        onTap: () {
          _sweetSheet.show(
              context: context,
              description: Text(
                'Que quieres hacer?',
                style: TextStyle(color: Color(0xff2D3748)),
              ),
              color: CustomSheetColor(
                main: Colors.white,
                accent: Colors.blue,
                icon: Colors.blue,
              ),
              icon: FontAwesomeIcons.info,
              actions: [
                SweetSheetAction(
                  onPressed: () async {
                    if (await canLaunch(
                        'https://www.google.com/maps/search/?api=1&query=' +
                            widget.supermarketEntity.latitude.toString() +
                            ',' +
                            widget.supermarketEntity.longitude.toString())) {
                      await launch(
                          'https://www.google.com/maps/search/?api=1&query=' +
                              widget.supermarketEntity.latitude.toString() +
                              ',' +
                              widget.supermarketEntity.longitude.toString());
                    } else {
                      throw 'Could not launch ' +
                          'https://www.google.com/maps/search/?api=1&query=' +
                          widget.supermarketEntity.latitude.toString() +
                          ',' +
                          widget.supermarketEntity.longitude.toString();
                    }
                  },
                  title: 'IR',
                ),
                SweetSheetAction(
                  onPressed: () async {
                    await launch('tel:' + widget.supermarketEntity.phone);
                  },
                  title: 'LLAMAR',
                ),
              ]);
        },
      ),
    );
  }
}
