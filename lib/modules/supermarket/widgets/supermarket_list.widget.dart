import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/supermarket/controllers/supermarket.controller.dart';
import 'package:ichillan_app/modules/supermarket/widgets/supermarket_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class SupermarketListWidget extends StatefulWidget {
  @override
  _SupermarketListWidgetState createState() => _SupermarketListWidgetState();
}

class _SupermarketListWidgetState extends State<SupermarketListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listSupermarket',
            init: SupermarketController(),
            builder: (___) => ListView.builder(
              itemCount: _.supermarkets.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.supermarkets[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.supermarkets[index].address.toLowerCase().contains(___.search.toLowerCase()))){
                  return SupermarketItemWidget(supermarketEntity: _.supermarkets[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
