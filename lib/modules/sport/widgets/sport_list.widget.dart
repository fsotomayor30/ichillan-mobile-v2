import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/sport/controllers/sport.controller.dart';
import 'package:ichillan_app/modules/sport/widgets/sport_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class SportListWidget extends StatefulWidget {
  @override
  _SportListWidgetState createState() => _SportListWidgetState();
}

class _SportListWidgetState extends State<SportListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listSport',
            init: SportController(),
            builder: (___) => ListView.builder(
              itemCount: _.sports.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.sports[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.sports[index].address.toLowerCase().contains(___.search.toLowerCase())) ||(_.sports[index].type.toLowerCase().contains(___.search.toLowerCase()))){
                  return SportItemWidget(sportEntity: _.sports[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
