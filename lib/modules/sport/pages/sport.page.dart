import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/sport/widgets/input_search_sport.widget.dart';
import 'package:ichillan_app/modules/sport/widgets/sport_list.widget.dart';

class SportPage extends StatefulWidget {
  @override
  _SportPageState createState() => _SportPageState();
}

class _SportPageState extends State<SportPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Deportes', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchSportWidget(),
          Expanded(
            child:SportListWidget(),
          )
        ],
      ),
    );
  }
}
