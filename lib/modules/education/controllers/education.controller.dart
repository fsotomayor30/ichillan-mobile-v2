import 'package:get/get.dart';

class EducationController extends GetxController{
  String _search;
  String get search => _search;

  void setSearch(String search){
    _search = search;
    update(['listEducation']);
  }
}