import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/education/widgets/education_list.widget.dart';
import 'package:ichillan_app/modules/education/widgets/input_search_education.widget.dart';

class EducationPage extends StatefulWidget {
  @override
  _EducationPageState createState() => _EducationPageState();
}

class _EducationPageState extends State<EducationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Educación', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchEducationWidget(),
          Expanded(
            child:EducationListWidget(),
          )
        ],
      ),
    );
  }
}
