import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/education/controllers/education.controller.dart';
import 'package:ichillan_app/modules/education/widgets/education_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class EducationListWidget extends StatefulWidget {
  @override
  _EducationListWidgetState createState() => _EducationListWidgetState();
}

class _EducationListWidgetState extends State<EducationListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listEducation',
            init: EducationController(),
            builder: (___) => ListView.builder(
              itemCount: _.educations.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.educations[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.educations[index].address.toLowerCase().contains(___.search.toLowerCase())) ||(_.educations[index].type.toLowerCase().contains(___.search.toLowerCase()))){
                  return EducationItemWidget(educationEntity: _.educations[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
