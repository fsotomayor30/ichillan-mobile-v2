import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/park/controllers/park.controller.dart';
import 'package:ichillan_app/modules/park/widgets/park_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class ParkListWidget extends StatefulWidget {
  @override
  _ParkListWidgetState createState() => _ParkListWidgetState();
}

class _ParkListWidgetState extends State<ParkListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listPark',
            init: ParkController(),
            builder: (___) => ListView.builder(
              itemCount: _.parks.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.parks[index].name.toLowerCase().contains(___.search.toLowerCase())) ||(_.parks[index].address.toLowerCase().contains(___.search.toLowerCase()))){
                  return ParkItemWidget(parkEntity: _.parks[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
