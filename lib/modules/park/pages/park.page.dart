
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/park/widgets/input_search_park.widget.dart';
import 'package:ichillan_app/modules/park/widgets/park_list.widget.dart';

class ParkPage extends StatefulWidget {
  @override
  _ParkPageState createState() => _ParkPageState();
}

class _ParkPageState extends State<ParkPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Plazas', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchParkWidget(),
          Expanded(
            child:ParkListWidget(),
          )
        ],
      ),
    );
  }
}
