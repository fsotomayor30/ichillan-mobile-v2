import 'package:flutter/cupertino.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/cloud_firestore/banners.function.dart';
import 'package:ichillan_app/cloud_firestore/cultures.function.dart';
import 'package:ichillan_app/cloud_firestore/educations.function.dart';
import 'package:ichillan_app/cloud_firestore/emergencies.function.dart';
import 'package:ichillan_app/cloud_firestore/entertainment.function.dart';
import 'package:ichillan_app/cloud_firestore/entrepreneurships.function.dart';
import 'package:ichillan_app/cloud_firestore/hotels.function.dart';
import 'package:ichillan_app/cloud_firestore/parks.function.dart';
import 'package:ichillan_app/cloud_firestore/qualifies.function.dart';
import 'package:ichillan_app/cloud_firestore/restaurants.function.dart';
import 'package:ichillan_app/cloud_firestore/sports.function.dart';
import 'package:ichillan_app/cloud_firestore/supermarkets.function.dart';
import 'package:ichillan_app/cloud_firestore/vets.function.dart';
import 'package:ichillan_app/entities/banner.entity.dart';
import 'package:ichillan_app/entities/culture.entity.dart';
import 'package:ichillan_app/entities/education.entity.dart';
import 'package:ichillan_app/entities/emergency.entity.dart';
import 'package:ichillan_app/entities/entertainment.entity.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';
import 'package:ichillan_app/entities/hotel.entity.dart';
import 'package:ichillan_app/entities/park.entity.dart';
import 'package:ichillan_app/entities/pharmacy.entity.dart';
import 'package:ichillan_app/entities/qualify.entity.dart';
import 'package:ichillan_app/entities/restaurant.entity.dart';
import 'package:ichillan_app/entities/sport.entity.dart';
import 'package:ichillan_app/entities/supermarket.entity.dart';
import 'package:ichillan_app/entities/veterinary.entity.dart';
import 'package:ichillan_app/http/fuel_station.service.dart';
import 'package:ichillan_app/http/pharmacy.service.dart';
import 'package:ichillan_app/modules/home/pages/home.page.dart';
import 'package:ichillan_app/modules/onboarding/pages/introduction_slider.page.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashController extends GetxController {
  GlobalController _globalController = Get.find();
  ParkFunction _parkFunction = ParkFunction();
  SportFunction _sportFunction = SportFunction();
  EducationFunction _educationFunction = EducationFunction();
  EntertainmentFunction _entertainmentFunction = EntertainmentFunction();
  HotelFunction _hotelsFunction = HotelFunction();
  RestaurantFunction _restaurantFunction = RestaurantFunction();
  CultureFunction _cultureFunction = CultureFunction();
  SupermarketFunction _supermarketFunction = SupermarketFunction();
  VeterinaryFunction _veterinaryFunction = VeterinaryFunction();
  FuelStationService _fuelStationService = FuelStationService();
  PharmacyService _pharmacyService = PharmacyService();
  EmergencyFunction _emergencyFunction = EmergencyFunction();
  EntrepreneurshipFunction _entrepreneurshipFunction =
      EntrepreneurshipFunction();
  BannerFunction _bannerFunction = BannerFunction();
  QualifyFunction _qualifyFunction = QualifyFunction();

  @override
  void onReady() async {
    List<ParkEntity> parks = await _parkFunction.getAllParks();
    _globalController.setParksList(parks);
    List<SportEntity> sports = await _sportFunction.getAllSports();
    _globalController.setSportsList(sports);
    List<EducationEntity> educations =
        await _educationFunction.getAllEducation();
    _globalController.setEducationsList(educations);
    List<EntertainmentEntity> entertainments =
        await _entertainmentFunction.getAllEntertainment();
    _globalController.setEntertainmentList(entertainments);
    List<HotelEntity> hotels = await _hotelsFunction.getAllHotels();
    _globalController.setHotelsList(hotels);
    List<RestaurantEntity> restaurants =
        await _restaurantFunction.getAllRestaurants();
    _globalController.setRestaurantList(restaurants);
    List<CultureEntity> cultures = await _cultureFunction.getAllCultures();
    _globalController.setCultureList(cultures);
    List<SupermarketEntity> supermarkets =
        await _supermarketFunction.getAllSupermarkets();
    _globalController.setSupermarketList(supermarkets);
    List<VeterinaryEntity> vets = await _veterinaryFunction.getAllVets();
    _globalController.setVeterinaryList(vets);
    List<FuelStationEntity> fuelStations =
        await _fuelStationService.getAllFuelStations();
    _globalController.setFuelStationList(fuelStations);
    List<PharmacyEntity> pharmacies = await _pharmacyService.getAllPharmacies();
    _globalController.setPharmacyList(pharmacies);
    List<EmergencyEntity> emergencylist =
        await _emergencyFunction.getAllEmergency();
    _globalController.setEmergencyList(emergencylist);
    List<EntrepreneurshipEntity> entrepreneurshiplist =
        await _entrepreneurshipFunction.getAllentrepreneurships();
    _globalController.setEntrepreneurshipList(entrepreneurshiplist);
    List<BannerEntity> bannerList = await _bannerFunction.getAllBannersActive();
    _globalController.setBannerList(bannerList);
    List<QualifyEntity> qualifyList = await _qualifyFunction.getAllQualifies();
    _globalController.setQualifyList(qualifyList);
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

    try {
      String imei = await FlutterUdid.consistentUdid;
      _globalController.setUserIMEI(imei);
    } on Exception catch (_) {
      print('Failed to get platform version.');
    }

    final SharedPreferences prefs = await _prefs;
    bool showOnboarding = prefs.getBool('showOnboarding');

    print("SPLASH " + showOnboarding.toString());
    if (showOnboarding != null) {
      if (showOnboarding) {
        Get.offAll(() => HomePage());
      } else {
        precacheImage(AssetImage(CARRUSEL1_ICON), Get.overlayContext);
        precacheImage(AssetImage(CARRUSEL2_ICON), Get.overlayContext);
        precacheImage(AssetImage(CARRUSEL3_ICON), Get.overlayContext);
        Get.offAll(() => IntroductionSliderPage());
      }
    } else {
      precacheImage(AssetImage(CARRUSEL1_ICON), Get.overlayContext);
      precacheImage(AssetImage(CARRUSEL2_ICON), Get.overlayContext);
      precacheImage(AssetImage(CARRUSEL3_ICON), Get.overlayContext);
      Get.offAll(() => IntroductionSliderPage());
    }

    super.onReady();
  }
}
