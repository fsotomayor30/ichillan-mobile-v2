
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/splash/controllers/splash.controller.dart';
import 'package:ichillan_app/modules/splash/widgets/image_splash.widget.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: SplashController(),
    builder: (_) => Scaffold(
      body: Container(
        height: Get.height,
        width: Get.width,
        color: COLOR_PRIMARY,
        child: Center(
          child: ImageSplashWidget(),
        ),
      ),
    ));
  }
}
