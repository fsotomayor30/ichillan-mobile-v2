import 'package:flutter/material.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';

class ImageSplashWidget extends StatefulWidget {
  @override
  _ImageSplashWidgetState createState() => _ImageSplashWidgetState();
}

class _ImageSplashWidgetState extends State<ImageSplashWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Image.asset(SPLASH_ICON),
    );
  }
}
