import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_list.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/input_search_entrepreneurship.widget.dart';

class EntrepreneurshipPage extends StatefulWidget {
  @override
  _EntrepreneurshipPageState createState() => _EntrepreneurshipPageState();
}

class _EntrepreneurshipPageState extends State<EntrepreneurshipPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Emprendimientos', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          InputSearchEntrepreneurshipWidget(),
          Expanded(
            child:EntrepreneurshipListWidget(),
          )
        ],
      ),
    );
  }
}
