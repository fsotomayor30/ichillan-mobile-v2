import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_contact_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_description_detail_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_image_detail_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_title_detail_item.widget.dart';

class EntrepreneurshipDetailPage extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipDetailPage({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipDetailPageState createState() => _EntrepreneurshipDetailPageState();
}

class _EntrepreneurshipDetailPageState extends State<EntrepreneurshipDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Emprendimientos', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: ListView(
        children: [
          EntrepreneurshipImageDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
          EntrepreneurshipTitleDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
          EntrepreneurshipDescriptionDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
          EntrepreneurshipContactItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity)
        ],
      ),
    );
  }
}
