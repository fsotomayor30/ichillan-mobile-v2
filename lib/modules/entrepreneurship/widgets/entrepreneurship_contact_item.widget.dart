import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:url_launcher/url_launcher.dart';

class EntrepreneurshipContactItemWidget extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipContactItemWidget({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipContactItemWidgetState createState() => _EntrepreneurshipContactItemWidgetState();
}

class _EntrepreneurshipContactItemWidgetState extends State<EntrepreneurshipContactItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: Row(
          children: [
            Visibility(
                visible: widget.entrepreneurshipEntity.instagramUrl != "",
                child: GestureDetector(
                    onTap: () async {
                      if (await canLaunch('https://www.instagram.com/' +
                          widget.entrepreneurshipEntity.instagramUrl)) {
                        await launch('https://www.instagram.com/' +
                            widget.entrepreneurshipEntity.instagramUrl);
                      } else {
                        throw 'Could not launch ' +
                            'https://www.instagram.com/' +
                            widget.entrepreneurshipEntity.instagramUrl;
                      }
                    },
                    child: FaIcon(FontAwesomeIcons.instagram, size: 30, color: COLOR_PRIMARY)
                )
            ),
            Visibility(
                visible: widget.entrepreneurshipEntity.instagramUrl != "",
                child: SizedBox(width: 15)
            ),
            Visibility(
                visible: widget.entrepreneurshipEntity.facebookUrl != "",
                child: GestureDetector(
                    onTap: () async {
                      if (await canLaunch('https://www.facebook.com/' +
                          widget.entrepreneurshipEntity.facebookUrl)) {
                        await launch('https://www.facebook.com/' +
                            widget.entrepreneurshipEntity.facebookUrl);
                      } else {
                        throw 'Could not launch ' +
                            'https://www.facebook.com/' +
                            widget.entrepreneurshipEntity.facebookUrl;
                      }
                    },
                    child: FaIcon(FontAwesomeIcons.facebookSquare, size: 30, color: COLOR_PRIMARY)
                )
            ),
            Visibility(
                visible: widget.entrepreneurshipEntity.facebookUrl != "",
                child: SizedBox(width: 15)
            ),
            Visibility(
                visible: widget.entrepreneurshipEntity.phone != "",
                child: GestureDetector(
                    onTap: () async {
                      var whatsappUrl = "whatsapp://send?phone=" + widget.entrepreneurshipEntity.phone;

                      if (await canLaunch(whatsappUrl)) {
                        await launch(whatsappUrl);
                      } else {
                        Get.defaultDialog(
                            buttonColor: COLOR_PRIMARY,
                            confirmTextColor: Colors.white,
                            onConfirm: () => Get.back(),
                            title: 'Necesitas instalar whatsapp',
                            content: Text('')
                        );
                      }
                    },
                    child: FaIcon(FontAwesomeIcons.whatsapp, size: 30, color: COLOR_PRIMARY)
                )
            ),
          ],
        )
    );
  }
}
