import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';

class EntrepreneurshipDescriptionDetailItemWidget extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipDescriptionDetailItemWidget({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipDescriptionDetailItemWidgetState createState() => _EntrepreneurshipDescriptionDetailItemWidgetState();
}

class _EntrepreneurshipDescriptionDetailItemWidgetState extends State<EntrepreneurshipDescriptionDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Text(widget.entrepreneurshipEntity.description, textAlign: TextAlign.justify,style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300),),
    );
  }
}
