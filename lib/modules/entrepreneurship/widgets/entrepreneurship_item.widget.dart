import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_contact_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_description_detail_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_image_detail_item.widget.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_title_detail_item.widget.dart';

class EntrepreneurshipItemWidget extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;

  EntrepreneurshipItemWidget({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipItemWidgetState createState() => _EntrepreneurshipItemWidgetState();
}

class _EntrepreneurshipItemWidgetState extends State<EntrepreneurshipItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        EntrepreneurshipImageDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
        EntrepreneurshipTitleDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
        EntrepreneurshipDescriptionDetailItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity),
        EntrepreneurshipContactItemWidget(entrepreneurshipEntity: widget.entrepreneurshipEntity)
      ],
    );
  }
}
