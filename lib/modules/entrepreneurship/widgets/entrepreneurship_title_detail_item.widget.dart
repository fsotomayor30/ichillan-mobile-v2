import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';

class EntrepreneurshipTitleDetailItemWidget extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipTitleDetailItemWidget({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipTitleDetailItemWidgetState createState() => _EntrepreneurshipTitleDetailItemWidgetState();
}

class _EntrepreneurshipTitleDetailItemWidgetState extends State<EntrepreneurshipTitleDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: Text(widget.entrepreneurshipEntity.name, style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600)),
      ),
    );
  }
}
