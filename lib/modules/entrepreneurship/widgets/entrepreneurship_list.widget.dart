import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/entrepreneurship/controllers/entrepreneurship.controller.dart';
import 'package:ichillan_app/modules/entrepreneurship/widgets/entrepreneurship_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class EntrepreneurshipListWidget extends StatefulWidget {
  @override
  _EntrepreneurshipListWidgetState createState() => _EntrepreneurshipListWidgetState();
}

class _EntrepreneurshipListWidgetState extends State<EntrepreneurshipListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listEntrepreneurship',
            init: EntrepreneurshipController(),
            builder: (___) => ListView.builder(
              itemCount: _.entrepreneurships.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.entrepreneurships[index].name.toLowerCase().contains(___.search.toLowerCase()))||(_.entrepreneurships[index].description.toLowerCase().contains(___.search.toLowerCase()))){
                  return EntrepreneurshipItemWidget(entrepreneurshipEntity: _.entrepreneurships[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
