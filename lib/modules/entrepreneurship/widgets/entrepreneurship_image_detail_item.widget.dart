import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';

class EntrepreneurshipImageDetailItemWidget extends StatefulWidget {
  final EntrepreneurshipEntity entrepreneurshipEntity;
  EntrepreneurshipImageDetailItemWidget({@required this.entrepreneurshipEntity});

  @override
  _EntrepreneurshipImageDetailItemWidgetState createState() => _EntrepreneurshipImageDetailItemWidgetState();
}

class _EntrepreneurshipImageDetailItemWidgetState extends State<EntrepreneurshipImageDetailItemWidget> {

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(autoPlay: false, aspectRatio: 1.0, enlargeCenterPage: true, enlargeStrategy: CenterPageEnlargeStrategy.height),
      items: [
        Container(
          child: Container(
            height: 302,
            width: 302,
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: widget.entrepreneurshipEntity.photo1,
                    fit: BoxFit.cover,
                    width: 302.0,
                    height: 315,
                    placeholder: (context, url) => Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                  ),

                ],
              ),
            ),
          ),
        ),
        Container(
          child: Container(
            height: 302,
            width: 302,
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: widget.entrepreneurshipEntity.photo2,
                    fit: BoxFit.cover,
                    width: 302.0,
                    height: 315,
                    placeholder: (context, url) => Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                  ),

                ],
              ),
            ),
          ),
        ),
        Container(
          child: Container(
            height: 302,
            width: 302,
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: widget.entrepreneurshipEntity.photo3,
                    fit: BoxFit.cover,
                    width: 302.0,
                    height: 315,
                    placeholder: (context, url) => Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
