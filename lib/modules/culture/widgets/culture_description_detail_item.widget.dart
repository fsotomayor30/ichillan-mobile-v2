import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/culture.entity.dart';

class CultureDescriptionDetailItemWidget extends StatefulWidget {
  final CultureEntity cultureEntity;
  CultureDescriptionDetailItemWidget({@required this.cultureEntity});

  @override
  _CultureDescriptionDetailItemWidgetState createState() => _CultureDescriptionDetailItemWidgetState();
}

class _CultureDescriptionDetailItemWidgetState extends State<CultureDescriptionDetailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Text(widget.cultureEntity.description, textAlign: TextAlign.justify,style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w300),),
    );
  }
}
