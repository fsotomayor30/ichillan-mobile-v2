import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/culture/widgets/culture_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class CultureListWidget extends StatefulWidget {
  @override
  _CultureListWidgetState createState() => _CultureListWidgetState();
}

class _CultureListWidgetState extends State<CultureListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => ListView.builder(
          itemCount: _.cultures.length,
          shrinkWrap: false,
          padding: EdgeInsets.symmetric(vertical: 15),
          itemBuilder: (__, index){
            return CultureItemWidget(cultureEntity: _.cultures[index]);
          },
        ));
  }
}
