import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/culture.entity.dart';
import 'package:ichillan_app/modules/culture/widgets/culture_description_detail_item.widget.dart';
import 'package:ichillan_app/modules/culture/widgets/culture_image_detail_item.widget.dart';
import 'package:ichillan_app/modules/culture/widgets/culture_title_detail_item.widget.dart';

class CultureItemWidget extends StatefulWidget {
  final CultureEntity cultureEntity;
  CultureItemWidget({@required this.cultureEntity});

  @override
  _CultureItemWidgetState createState() => _CultureItemWidgetState();
}

class _CultureItemWidgetState extends State<CultureItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Column(
        children: [
          CultureImageDetailItemWidget(cultureEntity: widget.cultureEntity),
          CultureTitleDetailItemWidget(cultureEntity: widget.cultureEntity),
          CultureDescriptionDetailItemWidget(cultureEntity: widget.cultureEntity),
        ],
      ),
    );
  }
}
