import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ichillan_app/entities/culture.entity.dart';

class CultureImageDetailItemWidget extends StatefulWidget {
  final CultureEntity cultureEntity;
  CultureImageDetailItemWidget({@required this.cultureEntity});

  @override
  _CultureImageDetailItemWidgetState createState() => _CultureImageDetailItemWidgetState();
}

class _CultureImageDetailItemWidgetState extends State<CultureImageDetailItemWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: 302,
        width: 302,
        margin: EdgeInsets.all(5.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Stack(
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: widget.cultureEntity.image,
                fit: BoxFit.cover,
                width: 302.0,
                height: 315,
                placeholder: (context, url) => Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: new CircularProgressIndicator(),
                ),
                errorWidget: (context, url, error) => new Icon(Icons.error),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
