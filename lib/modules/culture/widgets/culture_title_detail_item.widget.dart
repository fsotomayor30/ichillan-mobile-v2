import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/culture.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class CultureTitleDetailItemWidget extends StatefulWidget {
  final CultureEntity cultureEntity;
  CultureTitleDetailItemWidget({@required this.cultureEntity});

  @override
  _CultureTitleDetailItemWidgetState createState() => _CultureTitleDetailItemWidgetState();
}

class _CultureTitleDetailItemWidgetState extends State<CultureTitleDetailItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(child: Text(widget.cultureEntity.name, style: TextStyle(color: Color(0xFF707070), fontWeight: FontWeight.w600))),
          SizedBox(width: 15),
          GestureDetector(
              onTap: (){
                _sweetSheet.show(
                  context: context,
                  description: Text(
                    'Que quieres hacer?',
                    style: TextStyle(color: Color(0xff2D3748)),
                  ),
                  color: CustomSheetColor(
                    main: Colors.white,
                    accent: Colors.blue,
                    icon: Colors.blue,
                  ),
                  icon: FontAwesomeIcons.info,
                  actions:
                  [
                  SweetSheetAction(
                    onPressed: () async {
                      if (await canLaunch(
                          'https://www.google.com/maps/search/?api=1&query=' +
                              widget.cultureEntity.latitude.toString() + ',' +
                              widget.cultureEntity.longitude.toString())) {
                        await launch(
                            'https://www.google.com/maps/search/?api=1&query=' +
                                widget.cultureEntity.latitude.toString() + ',' +
                                widget.cultureEntity.longitude.toString());
                      } else {
                        throw 'Could not launch ' +
                            'https://www.google.com/maps/search/?api=1&query=' +
                            widget.cultureEntity.latitude.toString() + ',' +
                            widget.cultureEntity.longitude.toString();
                      }
                    },
                    title: 'IR',
                  ),
                  ]
                );
              },
              child: FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY)
          )
        ],
      ),
    );
  }
}
