import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/culture/widgets/culture_list.widget.dart';

class CulturePage extends StatefulWidget {
  @override
  _CulturePageState createState() => _CulturePageState();
}

class _CulturePageState extends State<CulturePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Ruta Cultural', style: TextStyle(fontWeight: FontWeight.w400)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child:CultureListWidget(),
          )
        ],
      ),
    );
  }
}
