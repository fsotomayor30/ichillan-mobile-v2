import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ichillan_app/modules/onboarding/slide.dart';

class SlideItemWidget extends StatelessWidget {
  final int index;

  SlideItemWidget(this.index);

  @override
  Widget build(BuildContext context) {
    return slideList[index].child;
  }
}
