import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';

class SlideDotsWidget extends StatelessWidget {
  bool isActive;

  SlideDotsWidget(this.isActive);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 10),
      height: isActive ? 12 : 8,
      width: isActive ? 12 : 8,
      decoration: BoxDecoration(
        color: isActive ? COLOR_PRIMARY : Color(0xFFB8BBC6),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
