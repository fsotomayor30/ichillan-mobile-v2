import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Slide {
  final Column child;

  Slide({@required this.child});
}

final slideList = [
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "Somos una aplicación que conecta a las personas de la ciudad.",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
            color: Color(0xFF707070)
          ),
        ),
      ],
    ),
  ),
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "Aquí podrás encontrar productos o servicios que estén dentro de tu ciudad.",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
            color: Color(0xFF707070)
          ),
        ),
      ],
    ),
  ),
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "También calificar los servicios obtenidos.",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 16, color: Color(0xFF707070),
        ),)
      ],
    ),
  )
];
