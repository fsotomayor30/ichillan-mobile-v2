import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/home/pages/home.page.dart';
import 'package:ichillan_app/modules/onboarding/slide.dart';
import 'package:ichillan_app/modules/onboarding/widgets/slide_dots.widget.dart';
import 'package:ichillan_app/modules/onboarding/widgets/slide_item.widget.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/config/img.enums.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntroductionSliderPage extends StatefulWidget {
  IntroductionSliderPage({Key key}) : super(key: key);

  @override
  IntroductionSliderPageState createState() =>
      new IntroductionSliderPageState();
}

class IntroductionSliderPageState extends State<IntroductionSliderPage> {
  int _currentPage = 0;
  final PageController _pageController = PageController(initialPage: 0);
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  DecorationImage backgroundChooser(int index) {
    switch (index) {
      case 0:
        return DecorationImage(
          image: AssetImage(CARRUSEL1_ICON),
          fit: BoxFit.cover,
        );
        break;
      case 1:
        return DecorationImage(
            image: AssetImage(CARRUSEL2_ICON), fit: BoxFit.cover);
        break;
      case 2:
        return DecorationImage(
            image: AssetImage(CARRUSEL3_ICON), fit: BoxFit.cover);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            child: Container(color: Colors.white),
          ),
          Positioned(
            top: 0,
            child: Container(
              width: width,
              height: height * .7,
              decoration: BoxDecoration(
                image: backgroundChooser(_currentPage),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(0.0),
                    bottomRight: Radius.circular(0.0)),
                color: Colors.white,
              ),
            ),
          ),
          Visibility(
            visible: _currentPage != 2,
            child: Positioned(
              right: 20,
              top: 40,
              child: GestureDetector(
                  onTap: () async {
                    final SharedPreferences prefs = await _prefs;
                    prefs.setBool('showOnboarding', true);
                    Get.offAll(() => HomePage());
                  },
                  child: Text(
                    'Saltar',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  )),
            ),
          ),
          Positioned(
            bottom: 40,
            left: 20,
            right: 20,
            child: Container(
              height: height * .2,
              width: width * .9,
              child: PageView.builder(
                scrollDirection: Axis.horizontal,
                onPageChanged: _onPageChanged,
                controller: _pageController,
                itemCount: slideList.length,
                itemBuilder: (context, i) => SlideItemWidget(i),
              ),
            ),
          ),
          Positioned(
            bottom: 15,
            right: 100,
            left: 100,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (int i = 0; i < slideList.length; i++)
                  if (i == _currentPage)
                    SlideDotsWidget(true)
                  else
                    SlideDotsWidget(false)
              ],
            ),
          ),
          Visibility(
            visible: _currentPage == 2,
            child: Positioned(
                bottom: 10,
                right: 15,
                child: GestureDetector(
                    onTap: () async {
                      final SharedPreferences prefs = await _prefs;
                      prefs.setBool('showOnboarding', true);
                      Get.offAll(() => HomePage());
                    },
                    child: FaIcon(
                      Icons.arrow_forward_ios,
                      color: COLOR_PRIMARY,
                    ))),
          ),
        ],
      ),
    );
  }
}
