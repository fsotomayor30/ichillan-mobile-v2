import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/help/controllers/help.controller.dart';
import 'package:ichillan_app/modules/help/widgets/help_item.widget.dart';
import 'package:ichillan_app/shared/controllers/global.controller.dart';

class HelpListWidget extends StatefulWidget {
  @override
  _HelpListWidgetState createState() => _HelpListWidgetState();
}

class _HelpListWidgetState extends State<HelpListWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (_) => GetBuilder(
            id: 'listEmergency',
            init: HelpController(),
            builder: (___) => ListView.builder(
              itemCount: _.emergencies.length,
              shrinkWrap: false,
              padding: EdgeInsets.symmetric(vertical: 15),
              itemBuilder: (__, index){
                if(___.search == null || ___.search.isEmpty ||(_.emergencies[index].name.toLowerCase().contains(___.search.toLowerCase()))){
                  return HelpItemWidget(emergencyEntity: _.emergencies[index]);
                }else{
                  return Container();
                }
              },
            )));
  }
}
