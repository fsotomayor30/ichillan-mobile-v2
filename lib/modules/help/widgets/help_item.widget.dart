import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ichillan_app/entities/emergency.entity.dart';
import 'package:ichillan_app/shared/config/colors.enums.dart';
import 'package:ichillan_app/shared/widgets/bottom_sheets.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpItemWidget extends StatefulWidget {
  final EmergencyEntity emergencyEntity;

  HelpItemWidget({@required this.emergencyEntity});

  @override
  _HelpItemWidgetState createState() => _HelpItemWidgetState();
}

class _HelpItemWidgetState extends State<HelpItemWidget> {
  final SweetSheet _sweetSheet = SweetSheet();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        leading: FaIcon(FontAwesomeIcons.exclamationTriangle, size: 40, color: COLOR_PRIMARY),
        title: Text(widget.emergencyEntity.name, style: TextStyle(fontSize: 14, color: Color(0xFF707070)),),
        trailing: FaIcon(FontAwesomeIcons.ellipsisV, size: 30, color: COLOR_PRIMARY),
        onTap: (){
          _sweetSheet.show(
            context: context,
            description: Text(
              'Que quieres hacer?',
              style: TextStyle(color: Color(0xff2D3748)),
            ),
            color: CustomSheetColor(
              main: Colors.white,
              accent: Colors.blue,
              icon: Colors.blue,
            ),
            icon: FontAwesomeIcons.info,
            actions: [
              SweetSheetAction(
                onPressed: () async {
                  print(widget.emergencyEntity.phone);
                  await launch('tel:' + widget.emergencyEntity.phone);
                },
                title: 'LLAMAR',
              )
            ],
          );
        },
      ),
    );
  }
}
