import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichillan_app/modules/help/controllers/help.controller.dart';
import 'package:ichillan_app/modules/help/widgets/help_list.widget.dart';
import 'package:ichillan_app/modules/help/widgets/input_search_emergency.widget.dart';

class HelpPage extends StatefulWidget {
  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: HelpController(),
        builder: (_) =>  Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text('Emergencias', style: TextStyle(fontWeight: FontWeight.w400)),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Colors.white),
              onPressed: () => Get.back(),
            ),
          ),
          body: Column(
            children: [
              InputSearchEmergencyWidget(),
              Expanded(
                child:HelpListWidget(),
              )
            ],
          ),
        ));
  }
}
