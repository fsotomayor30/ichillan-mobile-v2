import 'dart:convert';

import 'package:http/http.dart';
import 'package:ichillan_app/entities/emergency_pharmacy.entity.dart';
import 'package:ichillan_app/entities/pharmacy.entity.dart';

class PharmacyService {
  final Client _client = Client();

  Future<List<PharmacyEntity>> getAllPharmacies() async {
    String endpoint = 'https://farmanet.minsal.cl/index.php/ws/getLocales';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);

    final List<PharmacyEntity> result = [];
    responseJsonList.forEach((data) {
      if (data['comuna_nombre'] == "CHILLAN" ||
          data['comuna_nombre'] == "CHILLAN VIEJO") {
        if (data['local_lat'].toString().isNotEmpty) {
          List<String> temp =
              data['local_lat'].toString().replaceFirst(',', '.').split('.');
          data['local_lat'] = temp[0] + "." + temp[1];
        }
        if (data['local_lng'].toString().isNotEmpty) {
          List<String> temp =
              data['local_lng'].toString().replaceFirst(',', '.').split('.');
          data['local_lng'] = temp[0] + "." + temp[1];
        }
        final dataTemp = PharmacyEntity.fromJson(data);

        result.add(dataTemp);
      }
    });

    return result;
  }

  Future<EmergencyPharmacyEntity> getEmergencyPharmacy(String commune) async {
    String endpoint =
        'https://farmanet.minsal.cl/index.php/ws/getLocalesTurnos';

    final Response response = await _client.get(Uri.parse(endpoint));
    final responseJsonList = jsonDecode(response.body);

    final List<EmergencyPharmacyEntity> result = [];
    responseJsonList.forEach((data) {
      if (data['local_lat'].toString().isNotEmpty && data['local_lng'].toString().isNotEmpty) {
        List<String> templat =
            data['local_lat'].toString().replaceFirst(',', '.').split('.');

        List<String> templen =
        data['local_lng'].toString().replaceFirst(',', '.').split('.');

        if(templat.length == 1 && templen.length ==1){
          data['local_lat'] = templat[0] + "." + templat[1];
          data['local_lng'] = templen[0] + "." + templen[1];

        }

      }

      final dataTemp = EmergencyPharmacyEntity.fromJson(data);

      result.add(dataTemp);
    });

    return result.firstWhere((element) => element.comuna == commune,
        orElse: () => null);
  }
}
