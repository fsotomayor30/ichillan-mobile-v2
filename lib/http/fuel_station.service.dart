import 'dart:convert';

import 'package:http/http.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';

class FuelStationService {
  final Client _client = Client();

  Future<List<FuelStationEntity>> getAllFuelStations() async {
    String endpoint =
        'https://api.cne.cl/v3/combustibles/vehicular/estaciones?token=qv0ItzoRG5&comuna=16101';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);

    final List<FuelStationEntity> result = [];
    responseJsonList['data'].forEach((data) {
      final dataTemp = FuelStationEntity.fromJson(data);

      result.add(dataTemp);
    });

    return result;
  }

  Future<FuelStationEntity> getFuelStation93() async {
    String endpoint =
        'https://api.cne.cl/v3/combustibles/vehicular/estaciones?token=qv0ItzoRG5&comuna=16101';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);

    final List<FuelStationEntity> result = [];
    responseJsonList['data'].forEach((data) {
      final dataTemp = FuelStationEntity.fromJson(data);
      result.add(dataTemp);
    });

    result.sort(
        (a, b) => Comparable.compare(a.precios.precio93, b.precios.precio93));
    result.removeWhere((element) => element.precios.precio93 == 0);

    return result[0];
  }

  Future<FuelStationEntity> getFuelStation95() async {
    String endpoint =
        'https://api.cne.cl/v3/combustibles/vehicular/estaciones?token=qv0ItzoRG5&comuna=16101';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);
    final List<FuelStationEntity> result = [];
    responseJsonList['data'].forEach((data) {
      final dataTemp = FuelStationEntity.fromJson(data);
      result.add(dataTemp);
    });

    result.sort(
        (a, b) => Comparable.compare(a.precios.precio95, b.precios.precio95));
    result.removeWhere((element) => element.precios.precio95 == 0);

    return result[0];
  }

  Future<FuelStationEntity> getFuelStation97() async {
    String endpoint =
        'https://api.cne.cl/v3/combustibles/vehicular/estaciones?token=qv0ItzoRG5&comuna=16101';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);
    final List<FuelStationEntity> result = [];
    responseJsonList['data'].forEach((data) {
      final dataTemp = FuelStationEntity.fromJson(data);
      result.add(dataTemp);
    });

    result.sort(
        (a, b) => Comparable.compare(a.precios.precio97, b.precios.precio97));
    result.removeWhere((element) => element.precios.precio97 == 0);

    return result[0];
  }
}
