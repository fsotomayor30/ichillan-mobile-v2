import 'dart:convert';

import 'package:http/http.dart';
import 'package:ichillan_app/entities/weather.entity.dart';

class WeatherService {
  final Client _client = Client();

  Future<WeatherEntity> getWeather() async {
    String endpoint =
        'http://api.weatherapi.com/v1/current.json?key=535f2e64c6e542f2a5b52115202804&q=-36.6066399,-72.1034393&lang=es';
    final Response response = await _client.get(Uri.parse(endpoint));

    final responseJsonList = jsonDecode(response.body);
    return WeatherEntity.fromJson(responseJsonList);
  }
}
