class ConditionWeatherEntity {
  final String icon;
  final String text;

  ConditionWeatherEntity({this.icon, this.text});

  factory ConditionWeatherEntity.fromJson(Map<String, dynamic> json) {
    return ConditionWeatherEntity(
      icon: json['icon'],
      text: json['text'],
    );
  }

  @override
  String toString() {
    return 'ConditionWeatherEntity{icon: $icon, text: $text}';
  }
}
