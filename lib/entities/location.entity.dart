class Location {
  final String latitud;
  final String longitud;

  Location({this.latitud, this.longitud});

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      latitud: json['latitud'].toString(),
      longitud: json['longitud'].toString(),
    );
  }

  @override
  String toString() {
    return 'Location{latitud: $latitud, longitud: $longitud}';
  }
}
