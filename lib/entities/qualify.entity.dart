class QualifyEntity {
  QualifyEntity({
    this.key,
    this.qualify,
    this.name,
    this.idUser,
    this.idLocal,
    this.date
  });

  String qualify;
  String name;
  String idUser;
  String idLocal;
  String key;
  String date;


  QualifyEntity.fromSnapshot(dynamic element)
      : key = element.id,
        qualify = element["calificacion"] == null ? null : element["calificacion"],
        idUser = element["idUsuario"] == null ? null : element["idUsuario"],
        name = element["nombreLocal"] == null ? null : element["nombreLocal"],
        date = element["fecha"] == null ? null : element["fecha"],
        idLocal = element["idLocal"] == null ? null : element["idLocal"];

  Map<String, dynamic> toJson() => {
    "calificacion": qualify,
    "nombreLocal": name,
    "idUsuario": idUser,
    "fecha": date,
    "idLocal" : idLocal
  };

  @override
  String toString() {
    return 'QualifyEntity{qualify: $qualify, name: $name, idUser: $idUser, idLocal: $idLocal, key: $key, date: $date}';
  }
}