class BannerEntity {
  BannerEntity({
    this.key,
    this.image,
    this.name,
    this.active
  });



  bool active;
  String name;
  String image;
  String key;


  BannerEntity.fromSnapshot(dynamic element)
      : key = element.id,
        active = element["activo"] == null ? null : element["activo"] == "si" ? true : false,
        name = element["nombre"] == null ? null : element["nombre"],
        image = element["url"] == null ? null : element["url"];

  @override
  String toString() {
    return 'BannerEntity{active: $active, name: $name, image: $image, key: $key}';
  }
}