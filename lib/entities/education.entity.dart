class EducationEntity {
  EducationEntity({
    this.address,
    this.key,
    this.latitude,
    this.longitude,
    this.name,
    this.type,
    this.phone
  });



  String address;
  double latitude;
  double longitude;
  String name;
  String phone;
  String type;
  String key;


  EducationEntity.fromSnapshot(dynamic element)
      : key = element.id,
        address = element["Dirección"] == null ? null : element["Dirección"],
        latitude = element["Latitud"] == null ? null : element["Latitud"].toDouble(),
        longitude = element["Longitud"] == null ? null : element["Longitud"].toDouble(),
        name = element["Nombre"] == null ? null : element["Nombre"],
        phone = element["Teléfono"] == null ? null : element["Teléfono"],
        type = element["Tipo"] == null ? null : element["Tipo"];

  @override
  String toString() {
    return 'EducationEntity{address: $address, latitude: $latitude, longitude: $longitude, name: $name, phone: $phone, type: $type, key: $key}';
  }
}