
import 'package:ichillan_app/entities/condition_weather.entity.dart';

class CurrentWeatherEntity {
  final String lastUpdate;
  final double temp;
  final ConditionWeatherEntity conditionWeather;

  CurrentWeatherEntity({this.lastUpdate, this.temp, this.conditionWeather});

  factory CurrentWeatherEntity.fromJson(Map<String, dynamic> json) {
    return CurrentWeatherEntity(
        lastUpdate: json['last_updated'],
        temp: json['temp_c'],
        conditionWeather: ConditionWeatherEntity.fromJson(json['condition']));
  }

  @override
  String toString() {
    return 'CurrentWeather{lastUpdate: $lastUpdate, temp: $temp, conditionWeather: $conditionWeather}';
  }
}
