class EmergencyPharmacyEntity {
  final String nombre;
  final String direccion;
  final double latitud;
  final double longitud;
  final String comuna;

  EmergencyPharmacyEntity(
      {this.nombre, this.direccion, this.latitud, this.longitud, this.comuna});

  factory EmergencyPharmacyEntity.fromJson(Map<String, dynamic> json) {
    return EmergencyPharmacyEntity(
        nombre: json['local_nombre'],
        direccion: json['local_direccion'],
        latitud: json['local_lat'].toString().isEmpty ? null : double.parse(json['local_lat'].toString()),
        longitud: json['local_lng'].toString().isEmpty ? null : double.parse(json['local_lng'].toString()),
        comuna: json['comuna_nombre']);
  }

  @override
  String toString() {
    return 'EmergencyPharmacyEntity{nombre: $nombre, direccion: $direccion, latitud: $latitud, longitud: $longitud, comuna: $comuna}';
  }
}
