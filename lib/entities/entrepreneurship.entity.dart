class EntrepreneurshipEntity {
  EntrepreneurshipEntity({
    this.description,
    this.key,
    this.facebookUrl,
    this.photo1,
    this.photo2,
    this.photo3,
    this.photoPrincipal,
    this.instagramUrl,
    this.name,
    this.phone
  });

  String description;
  String facebookUrl;
  String photo1;
  String photo2;
  String photo3;
  String photoPrincipal;
  String instagramUrl;
  String name;
  String phone;
  String key;


  EntrepreneurshipEntity.fromSnapshot(dynamic element)
      : key = element.id,
        description = element["descripcion"] == null ? null : element["descripcion"],
        facebookUrl = element["facebookUrl"] == null ? null : element["facebookUrl"],
        photo1 = element["foto1"] == null ? null : element["foto1"],
        photo2 = element["foto2"] == null ? null : element["foto2"],
        photo3 = element["foto3"] == null ? null : element["foto3"],
        photoPrincipal = element["fotoPrincipal"] == null ? null : element["fotoPrincipal"],
        instagramUrl = element["instagramUrl"] == null ? null : element["instagramUrl"],
        name = element["nombre"] == null ? null : element["nombre"],
        phone = element["numeroContacto"] == null ? null : element["numeroContacto"];

}