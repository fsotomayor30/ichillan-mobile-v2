class PharmacyEntity {
  final String nombre;
  final String direccion;
  final double latitud;
  final double longitud;
  final String comuna;
  final String telefono;

  PharmacyEntity(
      {this.nombre, this.direccion, this.latitud, this.longitud, this.comuna, this.telefono});

  factory PharmacyEntity.fromJson(Map<String, dynamic> json) {
    return PharmacyEntity(
        nombre: json['local_nombre'],
        direccion: json['local_direccion'],
        latitud: json['local_lat'].toString().isEmpty ? null : double.parse(json['local_lat'].toString()),
        longitud: json['local_lng'].toString().isEmpty ? null : double.parse(json['local_lng'].toString()),
        comuna: json['comuna_nombre'],
        telefono: json['local_telefono'] == null ? null : json['local_telefono']);

  }

  @override
  String toString() {
    return 'PharmacyEntity{nombre: $nombre, direccion: $direccion, latitud: $latitud, longitud: $longitud, comuna: $comuna}';
  }
}
