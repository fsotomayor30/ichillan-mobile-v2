class BikesLanesEntity {
  final double point1;
  final double point2;
  final double point3;
  final double point4;

  BikesLanesEntity({this.point1, this.point2, this.point3, this.point4});

  BikesLanesEntity.fromSnapshot(dynamic element)
      : point1 = element["point1"] == null ? null : double.parse(element["point1"].toString()),
        point2 = element["point2"] == null ? null : double.parse(element["point2"].toString()),
        point3 = element["point3"] == null ? null : double.parse(element["point3"].toString()),
        point4 = element["point4"] == null ? null : double.parse(element["point4"].toString());

  @override
  String toString() {
    return 'BikesLanesEntity{point1: $point1, point2: $point2, point3: $point3, point4: $point4}';
  }
}
