class ParkEntity {
  ParkEntity({
    this.address,
    this.key,
    this.latitude,
    this.longitude,
    this.name
  });



  String address;
  double latitude;
  double longitude;
  String name;
  String key;


  ParkEntity.fromSnapshot(dynamic element)
      : key = element.id,
        address = element["Dirección"] == null ? null : element["Dirección"],
        latitude = element["Latitud"] == null ? null : element["Latitud"].toDouble(),
        longitude = element["Longitud"] == null ? null : element["Longitud"].toDouble(),
        name = element["Nombre"] == null ? null : element["Nombre"];

  @override
  String toString() {
    return 'ParkEntity{address: $address, latitude: $latitude, longitude: $longitude, name: $name, key: $key}';
  }
}