class VeterinaryEntity {
  VeterinaryEntity({
    this.address,
    this.key,
    this.latitude,
    this.longitude,
    this.name,
    this.phone
  });



  String address;
  double latitude;
  double longitude;
  String name;
  String phone;
  String key;


  VeterinaryEntity.fromSnapshot(dynamic element)
      : key = element.id,
        address = element["Dirección"] == null ? null : element["Dirección"],
        latitude = element["Latitud"] == null ? null : double.parse(element["Latitud"].toString()),
        longitude = element["Longitud"] == null ? null : double.parse(element["Longitud"].toString()),
        name = element["Nombre"] == null ? null : element["Nombre"],
        phone = element["Teléfono"] == null ? null : element["Teléfono"];


}