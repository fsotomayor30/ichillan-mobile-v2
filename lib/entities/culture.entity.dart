class CultureEntity {
  CultureEntity({
    this.key,
    this.latitude,
    this.longitude,
    this.name,
    this.image,
    this.description
  });



  double latitude;
  double longitude;
  String name;
  String image;
  String key;
  String description;


  CultureEntity.fromSnapshot(dynamic element)
      : key = element.id,
        latitude = element["Latitud"] == null ? null : double.parse(element["Latitud"].toString()),
        longitude = element["Longitud"] == null ? null : double.parse(element["Longitud"].toString()),
        name = element["Nombre"] == null ? null : element["Nombre"],
        description = element["Descripción"] == null ? null : element["Descripción"],
        image = element["imagen"] == null ? null : element["imagen"];
}