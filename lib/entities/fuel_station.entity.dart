
import 'package:ichillan_app/entities/distributor_fuel_station.entity.dart';
import 'package:ichillan_app/entities/location.entity.dart';
import 'package:ichillan_app/entities/payment_fuel_station.entity.dart';
import 'package:ichillan_app/entities/price_fuel_station.entity.dart';

class FuelStationEntity {
  final DistributorFuelStation distribuidor;
  final String direccion;
  final String ultima_actualizacion;
  final Location ubicacion;
  final PaymentFuelStation mediosPago;
  final PriceFuelStation precios;

  FuelStationEntity(
      {this.distribuidor,
      this.direccion,
      this.ultima_actualizacion,
      this.ubicacion,
      this.mediosPago,
      this.precios});

  factory FuelStationEntity.fromJson(Map<String, dynamic> json) {
    return FuelStationEntity(
        distribuidor: DistributorFuelStation.fromJson(json['distribuidor']),
        mediosPago: PaymentFuelStation.fromJson(json['metodos_de_pago']),
        direccion: json['direccion_calle'] + " " + json['direccion_numero'],
        ubicacion: Location.fromJson(json['ubicacion']),
        ultima_actualizacion: json['fecha_hora_actualizacion'],
        precios: PriceFuelStation.fromJson(json['precios']));
  }

  @override
  String toString() {
    return 'Estacion{distribuidor: $distribuidor, direccion: $direccion, ultima_actualizacion: $ultima_actualizacion, ubicacion: $ubicacion, mediosPago: $mediosPago, precios: $precios}';
  }
}
