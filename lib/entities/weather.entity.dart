
import 'package:ichillan_app/entities/current_weather.entity.dart';
import 'package:ichillan_app/entities/location_weather.entity.dart';

class WeatherEntity {
  final LocationWeatherEntity locationWeather;
  final CurrentWeatherEntity currentWeather;

  WeatherEntity({this.locationWeather, this.currentWeather});

  factory WeatherEntity.fromJson(Map<String, dynamic> json) {
    return WeatherEntity(
      locationWeather: LocationWeatherEntity.fromJson(json['location']),
      currentWeather: CurrentWeatherEntity.fromJson(json['current']),
    );
  }


}
