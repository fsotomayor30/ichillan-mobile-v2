class LocationWeatherEntity {
  final String name;
  final String localTime;

  LocationWeatherEntity({this.name, this.localTime});

  factory LocationWeatherEntity.fromJson(Map<String, dynamic> json) {
    return LocationWeatherEntity(
      name: json['name'],
      localTime: json['localtime'],
    );
  }

  @override
  String toString() {
    return 'LocationWeatherEntity{name: $name, localTime: $localTime}';
  }
}
