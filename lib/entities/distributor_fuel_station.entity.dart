class DistributorFuelStation {
  final String nombre;
  final String logo;

  DistributorFuelStation({this.nombre, this.logo});

  factory DistributorFuelStation.fromJson(Map<String, dynamic> json) {
    return DistributorFuelStation(
        nombre: json['nombre'].toString(), logo: json['logo'].toString());
  }

  @override
  String toString() {
    return 'DistributorFuelStation{nombre: $nombre, logo: $logo}';
  }
}
