class PaymentFuelStation {
  final String efectivo;
  final String cheque;
  final String tarjetasBancarias;
  final String tarjetasGrandesTiendas;

  PaymentFuelStation(
      {this.efectivo,
      this.cheque,
      this.tarjetasBancarias,
      this.tarjetasGrandesTiendas});

  factory PaymentFuelStation.fromJson(Map<String, dynamic> json) {
    return PaymentFuelStation(
      efectivo: json['ejectivo'].toString(),
      cheque: json['cheque'].toString(),
      tarjetasBancarias: json['tarjetas bancarias'].toString(),
      tarjetasGrandesTiendas: json['tarjetas grandes tiendas'].toString(),
    );
  }

  @override
  String toString() {
    return 'PaymentFuelStation{efectivo: $efectivo, cheque: $cheque, tarjetasBancarias: $tarjetasBancarias, tarjetasGrandesTiendas: $tarjetasGrandesTiendas}';
  }
}
