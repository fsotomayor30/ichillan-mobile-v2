class EmergencyEntity {
  EmergencyEntity({
    this.key,
    this.name,
    this.phone
  });
  
  String name;
  String phone;
  String key;
  
  EmergencyEntity.fromSnapshot(dynamic element)
      : key = element.id,
        name = element["Nombre"] == null ? null : element["Nombre"],
        phone = element["Teléfono"] == null ? null : element["Teléfono"];
}