class PriceFuelStation {
  final int precio93;
  final int precio95;
  final int precio97;

  PriceFuelStation({this.precio93, this.precio95, this.precio97});

  factory PriceFuelStation.fromJson(Map<String, dynamic> json) {
    return PriceFuelStation(
      precio93: json.containsKey('gasolina 93')
          ? int.parse(json['gasolina 93'].toString())
          : 0,
      precio95: json.containsKey('gasolina 95')
          ? int.parse(json['gasolina 95'].toString())
          : 0,
      precio97: json.containsKey('gasolina 97')
          ? int.parse(json['gasolina 97'].toString())
          : 0,
    );
  }

  @override
  String toString() {
    return 'Precios{precio93: $precio93, precio95: $precio95, precio97: $precio97}';
  }
}
