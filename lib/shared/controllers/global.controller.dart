import 'dart:async';

import 'package:get/get.dart';
import 'package:ichillan_app/entities/banner.entity.dart';
import 'package:ichillan_app/entities/culture.entity.dart';
import 'package:ichillan_app/entities/education.entity.dart';
import 'package:ichillan_app/entities/emergency.entity.dart';
import 'package:ichillan_app/entities/entertainment.entity.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';
import 'package:ichillan_app/entities/fuel_station.entity.dart';
import 'package:ichillan_app/entities/hotel.entity.dart';
import 'package:ichillan_app/entities/park.entity.dart';
import 'package:ichillan_app/entities/pharmacy.entity.dart';
import 'package:ichillan_app/entities/qualify.entity.dart';
import 'package:ichillan_app/entities/restaurant.entity.dart';
import 'package:ichillan_app/entities/sport.entity.dart';
import 'package:ichillan_app/entities/supermarket.entity.dart';
import 'package:ichillan_app/entities/veterinary.entity.dart';
import 'package:latlong2/latlong.dart';

class GlobalController extends GetxController {
  StreamSubscription _getPositionSubscription;

  List<ParkEntity> _parks;
  List<ParkEntity> get parks => _parks;

  List<SportEntity> _sports;
  List<SportEntity> get sports => _sports;

  List<HotelEntity> _hotels;
  List<HotelEntity> get hotels => _hotels;

  List<EducationEntity> _educations;
  List<EducationEntity> get educations => _educations;

  List<EntertainmentEntity> _entertainments;
  List<EntertainmentEntity> get entertainments => _entertainments;

  List<RestaurantEntity> _restaurants;
  List<RestaurantEntity> get restaurants => _restaurants;

  List<CultureEntity> _cultures;
  List<CultureEntity> get cultures => _cultures;

  List<SupermarketEntity> _supermarkets;
  List<SupermarketEntity> get supermarkets => _supermarkets;

  List<VeterinaryEntity> _vets;
  List<VeterinaryEntity> get vets => _vets;

  List<FuelStationEntity> _fuelStations;
  List<FuelStationEntity> get fuelStations => _fuelStations;

  List<PharmacyEntity> _pharmacies;
  List<PharmacyEntity> get pharmacies => _pharmacies;

  List<EmergencyEntity> _emergencies;
  List<EmergencyEntity> get emergencies => _emergencies;

  List<EntrepreneurshipEntity> _entrepreneurships;
  List<EntrepreneurshipEntity> get entrepreneurships => _entrepreneurships;

  List<QualifyEntity> _qualifies;
  List<QualifyEntity> get qualifies => _qualifies;

  List<BannerEntity> _banners;
  List<BannerEntity> get banners => _banners;

  LatLng _usersCenter = LatLng(-36.6067183, -72.1033265);
  LatLng get usersCenter => _usersCenter;

  String _userIMEI;
  String get userIMEI => _userIMEI;

  @override
  void onInit() {
    super.onInit();
  }

  void setUsersCenter(LatLng usersCenter) {
    _usersCenter = usersCenter;
    update(['map']);
  }

  @override
  void onClose() {
    _getPositionSubscription?.cancel();
  }

  void setParksList(List<ParkEntity> parks) {
    _parks = parks;
  }

  void setEducationsList(List<EducationEntity> educations) {
    _educations = educations;
  }

  void setSportsList(List<SportEntity> sports) {
    _sports = sports;
  }

  void setHotelsList(List<HotelEntity> hotels) {
    _hotels = hotels;
  }

  void setEntertainmentList(List<EntertainmentEntity> entertainments) {
    _entertainments = entertainments;
  }

  void setRestaurantList(List<RestaurantEntity> restaurants) {
    _restaurants = restaurants;
  }

  void setCultureList(List<CultureEntity> cultures) {
    _cultures = cultures;
  }

  void setSupermarketList(List<SupermarketEntity> supermarkets) {
    _supermarkets = supermarkets;
  }

  void setVeterinaryList(List<VeterinaryEntity> vets) {
    _vets = vets;
  }

  void setFuelStationList(List<FuelStationEntity> fuelStations) {
    _fuelStations = fuelStations;
  }

  void setPharmacyList(List<PharmacyEntity> pharmacies) {
    _pharmacies = pharmacies;
  }

  void setEmergencyList(List<EmergencyEntity> emergencies) {
    _emergencies = emergencies;
  }

  void setBannerList(List<BannerEntity> banners) {
    _banners = banners;
  }

  void setQualifyList(List<QualifyEntity> qualifies) {
    _qualifies = qualifies;
    update();
  }

  void setEntrepreneurshipList(List<EntrepreneurshipEntity> entrepreneurships) {
    _entrepreneurships = entrepreneurships;
  }

  void setUserIMEI(String userIMEI) {
    _userIMEI = userIMEI;
  }
}
