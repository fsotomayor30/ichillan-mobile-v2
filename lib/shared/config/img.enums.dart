const String SPLASH_ICON = 'assets/splash/splash_icon.png';

const String TREE_ICON = 'assets/map/tree.png';
const String SPORT_ICON = 'assets/map/sport.png';
const String EDUCATION_ICON = 'assets/map/education.png';
const String PARTY_ICON = 'assets/map/party.png';
const String HOTEL_ICON = 'assets/map/hotel.png';
const String CULTURE_ICON = 'assets/map/culture.png';
const String VETERINARY_ICON = 'assets/map/veterinary.png';
const String PHARMACY_ICON = 'assets/map/pharmacy.png';

const String EMERGENCY_PHARMACY_ICON = 'assets/information/emergencyPharmacy.jpg';
const String WEATHER_ICON = 'assets/information/weather.jpeg';
const String GASOLINE_ICON = 'assets/information/gasoline.png';

const String QUALIFY_ICON = 'assets/profile/QUALIFY.jpg';

const String CARRUSEL1_ICON = 'assets/carrusel/carrusel1.jpg';
const String CARRUSEL2_ICON = 'assets/carrusel/carrusel2.jpg';
const String CARRUSEL3_ICON = 'assets/carrusel/carrusel3.jpg';
