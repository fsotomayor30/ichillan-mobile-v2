import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/culture.entity.dart';

class CultureFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('RutaCultural');


  Future<List<CultureEntity>> getAllCultures () async{
    List<CultureEntity> culturesList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        culturesList.add(CultureEntity.fromSnapshot(doc));

      });
    }

    return culturesList;
  }


}