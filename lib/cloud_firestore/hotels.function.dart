import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/hotel.entity.dart';

class HotelFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Moteles');


  Future<List<HotelEntity>> getAllHotels () async{
    List<HotelEntity> hotelsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        hotelsList.add(HotelEntity.fromSnapshot(doc));

      });
    }

    return hotelsList;
  }


}