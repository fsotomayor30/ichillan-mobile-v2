import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/qualify.entity.dart';

class QualifyFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Calificaciones');


  Future<List<QualifyEntity>> getAllQualifies () async{
    List<QualifyEntity> qualifiesList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        qualifiesList.add(QualifyEntity.fromSnapshot(doc));

      });
    }

    return qualifiesList;
  }

  Future<void> saveRating(QualifyEntity qualifyEntity) async {
    await collection.add(qualifyEntity.toJson());
  }


}