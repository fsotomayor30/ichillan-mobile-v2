import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/sport.entity.dart';

class SportFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Canchas');


  Future<List<SportEntity>> getAllSports () async{
    List<SportEntity> sportsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        sportsList.add(SportEntity.fromSnapshot(doc));

      });
    }

    return sportsList;
  }


}