import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/park.entity.dart';

class ParkFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Plazas');


  Future<List<ParkEntity>> getAllParks () async{
    List<ParkEntity> parksList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        parksList.add(ParkEntity.fromSnapshot(doc));

      });
    }

    return parksList;
  }


}