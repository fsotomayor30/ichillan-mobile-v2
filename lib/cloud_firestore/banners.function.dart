import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/banner.entity.dart';

class BannerFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Banners');


  Future<List<BannerEntity>> getAllBannersActive () async{
    List<BannerEntity> bannersList = [];

    QuerySnapshot querySnapshot = await collection
    .where('activo', isEqualTo: 'si')
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        bannersList.add(BannerEntity.fromSnapshot(doc));

      });
    }

    return bannersList;
  }


}