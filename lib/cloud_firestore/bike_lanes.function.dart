import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/bike_lanes.entity.dart';

class BikesLanesFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('BikeLanes');


  Future<List<BikesLanesEntity>> getAllBikesLanes () async{
    List<BikesLanesEntity> bikesLanesList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        bikesLanesList.add(BikesLanesEntity.fromSnapshot(doc));

      });
    }

    return bikesLanesList;
  }


}