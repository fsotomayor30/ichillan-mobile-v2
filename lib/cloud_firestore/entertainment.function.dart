import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/entertainment.entity.dart';

class EntertainmentFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Entretenimiento');


  Future<List<EntertainmentEntity>> getAllEntertainment () async{
    List<EntertainmentEntity> entertainmentsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        entertainmentsList.add(EntertainmentEntity.fromSnapshot(doc));

      });
    }

    return entertainmentsList;
  }


}