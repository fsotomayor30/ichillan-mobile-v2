import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/entrepreneurship.entity.dart';

class EntrepreneurshipFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Emprendimientos');


  Future<List<EntrepreneurshipEntity>> getAllentrepreneurships () async{
    List<EntrepreneurshipEntity> entrepreneurshipsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        entrepreneurshipsList.add(EntrepreneurshipEntity.fromSnapshot(doc));

      });
    }

    return entrepreneurshipsList;
  }


}