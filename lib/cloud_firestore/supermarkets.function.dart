import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/supermarket.entity.dart';

class SupermarketFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Supermercados');


  Future<List<SupermarketEntity>> getAllSupermarkets () async{
    List<SupermarketEntity> supermarketsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        supermarketsList.add(SupermarketEntity.fromSnapshot(doc));

      });
    }

    return supermarketsList;
  }


}