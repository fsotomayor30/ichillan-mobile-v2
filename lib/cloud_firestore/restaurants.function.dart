import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/restaurant.entity.dart';

class RestaurantFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Restaurant');


  Future<List<RestaurantEntity>> getAllRestaurants () async{
    List<RestaurantEntity> restaurantsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        restaurantsList.add(RestaurantEntity.fromSnapshot(doc));

      });
    }

    return restaurantsList;
  }


}