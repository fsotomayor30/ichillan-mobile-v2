import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/veterinary.entity.dart';

class VeterinaryFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Veterinaria');


  Future<List<VeterinaryEntity>> getAllVets () async{
    List<VeterinaryEntity> vetsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        vetsList.add(VeterinaryEntity.fromSnapshot(doc));

      });
    }

    return vetsList;
  }


}