import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/emergency.entity.dart';

class EmergencyFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Emergencia');


  Future<List<EmergencyEntity>> getAllEmergency () async{
    List<EmergencyEntity> emergenciesList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        emergenciesList.add(EmergencyEntity.fromSnapshot(doc));

      });
    }

    return emergenciesList;
  }


}