import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ichillan_app/entities/education.entity.dart';

class EducationFunction {
  final CollectionReference collection = FirebaseFirestore.instance.collection('Educacion');


  Future<List<EducationEntity>> getAllEducation () async{
    List<EducationEntity> educationsList = [];

    QuerySnapshot querySnapshot = await collection
        .get();

    if(querySnapshot.docs.length>0){
      querySnapshot.docs.forEach((doc) {
        educationsList.add(EducationEntity.fromSnapshot(doc));

      });
    }

    return educationsList;
  }


}